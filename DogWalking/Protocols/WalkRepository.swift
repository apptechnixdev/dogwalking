//
//  WalkRepository.swift
//  DogWalking
//
//  Created by Admin on 2021/01/04.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
protocol WalkRepository {
    func performStartWalk(params: [String: Any], completion:@escaping ((Error?)->Void))
    func performEndWalk(params: [String: Any], completion:@escaping ((Error?)->Void))
    func getWalkHistory(userId:String,completion:@escaping (([WalkHistory]?, Error?)->Void))


}
