//
//  LoginRepository.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol LoginRepository {
    func performLogin(params: [String:String], completion:@escaping ((UserDetails?, Error?)->Void))
}
