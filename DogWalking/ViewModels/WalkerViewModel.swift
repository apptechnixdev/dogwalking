//
//  WalkerViewModel.swift
//  DogWalking
//
//  Created by Admin on 2020/10/23.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class WalkerViewModel: NSObject {
    private var view : WalkersView?
    private let objScheduleRepository = ScheduleRepositoryImpl()
    
    init(view: WalkersView?) {
        self.view = view
    }

    func getWalkerNewRequest(controller: UIViewController, completion:@escaping() -> ()) {
        SharedClass.sharedInstance.showLoader()
        guard let memberId = UserManager.sharedInstance.getUserDetials().user_id else {
            return
        }
        objScheduleRepository.getWalkerNewRequest(userId: memberId) { (requests, error) in
            SharedClass.sharedInstance.dismissLoader()
            completion()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if let requests = requests {
                    self.view?.setWalkerNewRequestResults(requests: requests)
                }
            }
        }
    }
    
    func getWalkerInProgressRequest(controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        guard let memberId = UserManager.sharedInstance.getUserDetials().user_id else {
            return
        }
        objScheduleRepository.getWalkerInProgressRequest(userId: memberId) { (response, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if let response = response {
                    self.view?.setWalkerInProgressRequestResults(requests: response)
                }
            }
            
        }
    }
    
    func updateWalkerNewRequest(params: [String:String], controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objScheduleRepository.updateWalkerNewRequest(params: params) { (isRequestUpdated, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if isRequestUpdated {
                    self.view?.newRequestUpdated()
                }
            }
        }
    }


}
