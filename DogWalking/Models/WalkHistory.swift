
import Foundation
struct WalkHistory : Codable {
	let member_id : String?
	let date : String?
	let duration : String?
	let pet_id : String?
	let address : String?
	let route_picture : String?

	enum CodingKeys: String, CodingKey {

		case member_id = "member_id"
		case date = "date"
		case duration = "duration"
		case pet_id = "pet_id"
		case address = "address"
		case route_picture = "route_picture"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		member_id = try values.decodeIfPresent(String.self, forKey: .member_id)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		duration = try values.decodeIfPresent(String.self, forKey: .duration)
		pet_id = try values.decodeIfPresent(String.self, forKey: .pet_id)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		route_picture = try values.decodeIfPresent(String.self, forKey: .route_picture)
	}

}
