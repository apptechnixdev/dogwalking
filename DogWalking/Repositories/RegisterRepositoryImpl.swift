//
//  RegisterRepositoryImpl.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class RegisterRepositoryImpl: NSObject {
}
extension RegisterRepositoryImpl: RegisterRepository {
    func performUpdateUserDetails(params: [String : String], completion: @escaping ((UserDetails?, Error?) -> Void)) {
                guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(data: params, headers:["Authorization":"Bearer \(token)"], url: "apis/v1/update_user.php", service: .posts, method: .post)
        objNetwork.executePostQuery() {
            (result: Result<UserDetails,Error>) in
            switch result{
            case .success(let details):
                UserManager.sharedInstance.updateUserDetails(user: details)
                completion(details, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }


    }
    
    func performRegister(params: [String : String], completion: @escaping ((Error?) -> Void)) {
        let objNetwork = NetworkCall(data: params, url: "apis/v1/create_user.php", service: .posts, method: .post)
        objNetwork.executeNonResultQuery() {
            (result: EmptyResult<Error>) in
            switch result{
            case .success:
                completion(nil)
            case .failure(let error1):
                completion(error1)
            }
        }

    }
    
    }


