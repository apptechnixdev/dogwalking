//
//  PetManager.swift
//  DogWalking
//
//  Created by Admin on 2020/10/08.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
public class PetManger: NSObject {
    static let sharedInstance = PetManger()
    private var objPets : [Pet] = []
    
    func getPetDetial(petId: String) -> Pet? {
        if let index = objPets.firstIndex(where: {($0.id == petId)}) {
            return objPets[index]
        }
        return nil
    }
    
    func storePetsDetail(pet: [Pet]) {
        self.objPets = pet
    }
    
    func getAllPets() -> [Pet] {
        return objPets
    }

}
