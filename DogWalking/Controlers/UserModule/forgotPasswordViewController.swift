//
//  forgotPasswordViewController.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/23.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit


class forgotPasswordViewController: UIViewController {
    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    var objForgotpasswordViewModel = ForgotPasswordViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.addPadding(padding: .left(13))
        self.setLeftBarItemProfileImage()
        navigationController?.navigationBar.transparentNavigationBar()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.configureDefaultNavigationBar(title: "Reset Password")

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    func validateLoginFields() -> Bool {
        guard !txtEmail.text!.isEmptyStr
            else {
                txtEmail.showError(message:ValidationText.emailMessage)
            return false
        }
        guard txtEmail.isValidEmail(txtEmail.text ?? "") else {
            txtEmail.showError(message:ValidationText.emailMessage1)
            return false
        }
        return true
    }

        @IBAction func onBtnSubmitTapped(sender: Any) {
            if validateLoginFields(), let email = txtEmail.text{
                objForgotpasswordViewModel.performForgotRequest(email: email, controller: self)
            }
         
         }

}
