//
//  ForgotPasswordRepositoryImpl.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/28.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit
class ForgotPasswordRepositoryImpl: NSObject {
}

extension ForgotPasswordRepositoryImpl: ForgotPasswordRepository {
    func performForgotRequest(email:String, completion: @escaping ((Error?) -> Void)) {
        let objNetwork = NetworkCall(data: ["email":email], url: "apis/v1/send_reset_email.php", service: .posts, method: .post)
            objNetwork.executeNonResultQuery() {
                    (result: EmptyResult<Error>) in
                    switch result{
                    case .success:
                        completion(nil)
                    case .failure(let error1):
                        completion(error1)
                }
            }


    }
    
}

    
    



