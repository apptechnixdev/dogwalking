//
//  StartWalkViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/10/30.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class StartWalkViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var btnYourLocation: UIButton!
    @IBOutlet weak var viewYourLocation: UIView!
    var userAnnotationImage: UIImage?
    var userAnnotation: UserAnnotation?
    var accuracyRangeCircle: MKCircle?
    var polyline: MKPolyline?
    var isZooming: Bool?
    var isBlockingAutoZoom: Bool?
    var zoomBlockingTimer: Timer?
    var didInitialZoom: Bool?
    let timeLeftShapeLayer = CAShapeLayer()
    let bgShapeLayer = CAShapeLayer()
    var timeLeft: TimeInterval = 0
    var endTime: Date?
    var timeLabel =  UILabel()
    var timer = Timer()
    var walkerLocations: [CLLocation] = []
    var objWalkViewModel: WalkViewModel?
    var objCurrentReqest : WalkerRequest?
    // here you create your basic animation object to animate the strokeEnd
    let strokeIt = CABasicAnimation(keyPath: "strokeEnd")
    var objPet: Pet?

    private var isWalker: Bool {
        if SharedClass.sharedInstance.getUserRole() == .dogWalker {
            return true
        }
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.objWalkViewModel = WalkViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureNavigationBar(largeTitleColor: .black, backgoundColor: .white, tintColor: .white, title: objPet?.name ?? "" ,preferredLargeTitle: false)
        self.setLeftBarItemProfileImage()
        self.setUpMapDetails()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isWalker {
            LocationService.sharedInstance.stopUpdatingLocation()
            guard let ownerId = objCurrentReqest?.ownerId else {
                return
            }
            NotificationCenter.default.removeObserver(self)
            self.removeLocation(withID: "Locations/WalkingLocation/dogowner_\(ownerId)")
        }
        self.clearPolyline()
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
    }
    
    func setUpMapDetails() {
        self.mapView.delegate = self
        self.mapView.showsUserLocation = false
        if let currentLocation = LocationService.sharedInstance.currentLocation {
            self.accuracyRangeCircle = MKCircle(center: currentLocation.coordinate, radius: 50)
            self.mapView.addOverlay(self.accuracyRangeCircle!)
        }
        self.didInitialZoom = false

        self.userAnnotationImage = UIImage(named: "user_position_ball")!
        self.view.sendSubviewToBack(self.mapView)
        self.clearPolyline()

        
        NotificationCenter.default.addObserver(self, selector: #selector(showTurnOnLocationServiceAlert(notification:)), name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)

        if self.isWalker {
            NotificationCenter.default.addObserver(self, selector: #selector(updateMap(notification:)), name: Notification.Name(rawValue:"didUpdateLocation"), object: nil)

            let title = UserManager.sharedInstance.getUserDetials().address
            btnYourLocation.setTitle(title, for: .normal)
            LocationService.sharedInstance.startUpdatingLocation()
            self.drawBgShape()
            self.drawTimeLeftShape()
            self.addTimeLabel()
            // here you define the fromValue, toValue and duration of your animation
            strokeIt.fromValue = 0
            strokeIt.toValue = 1
            strokeIt.duration = timeLeft
            // add the animation to your timeLeftShapeLayer
            timeLeftShapeLayer.add(strokeIt, forKey: nil)
            // define the future end time by adding the timeLeft to now Date()
            endTime = Date().addingTimeInterval(timeLeft)
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
            self.viewYourLocation.isHidden = false
        }
        else  {
            fetchDogLocations()
        }
    }
    
    func fetchDogLocations() {
            self.viewYourLocation.isHidden = true

            guard let user_id = UserManager.sharedInstance.getUserDetials().user_id else {
                return
            }
            let ref = Database.database().reference(withPath: "Locations/WalkingLocation/dogowner_\(user_id)")

            ref.queryOrdered(byChild: "timeStamp").observe(.value, with: { snapshot in
            var newLocations: [CLLocation] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                   let walkerLocation = WalkingLocation(snapshot: snapshot) {
                    let location = CLLocation(latitude: walkerLocation.lat, longitude: walkerLocation.long)
                    newLocations.append(location)
                }
            }
            self.walkerLocations = newLocations
            self.updatePolylines(locations: self.walkerLocations)
                if self.walkerLocations.count > 0, let location = self.walkerLocations.last {
                    self.zoomTo(location: location)
                }
            })
        }

    @objc func showTurnOnLocationServiceAlert(notification: NSNotification) {
        let alert = UIAlertController(title: "Turn on Location Service", message: "To use location tracking feature of the app, please turn on the location service from the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func updateMap(notification: NSNotification) {
        if let userInfo = notification.userInfo{
            
            updatePolylines(locations: LocationService.sharedInstance.locationDataArray)

            if let newLocation = userInfo["location"] as? CLLocation{
                zoomTo(location: newLocation)
                let timestamp = NSDate().timeIntervalSince1970
                let randomString = NSUUID().uuidString
                guard let walkerEmail = UserManager.sharedInstance.getUserDetials().email else {
                    return
                }
                let groceryItem = WalkingLocation(name: randomString,
                                                  addedByUser: walkerEmail,lat: newLocation.coordinate.latitude, long: newLocation.coordinate.longitude, timeStamp: timestamp)
                guard let ownerId = objCurrentReqest?.ownerId else {
                    return
                }
                let ref = Database.database().reference(withPath: "Locations/WalkingLocation/dogowner_\(ownerId)")
                let locationRef = ref.child(randomString.lowercased())
                locationRef.setValue(groceryItem.toAnyObject())
            }
            
        }
    }

    func drawBgShape() {
        let height = UIScreen.main.bounds.size.height - 225
        bgShapeLayer.path = UIBezierPath(arcCenter: CGPoint(x: view.frame.midX , y: height), radius:
            40, startAngle: -90.degreesToRadians, endAngle: 270.degreesToRadians, clockwise: true).cgPath
        bgShapeLayer.strokeColor = UIColor.clear.cgColor
        bgShapeLayer.fillColor = UIColor(hex: 0xDFD1D6).cgColor
        
        bgShapeLayer.lineWidth = 5
        self.view.layer.addSublayer(bgShapeLayer)
    }
    
    func drawTimeLeftShape() {
        let height = UIScreen.main.bounds.size.height - 225
        timeLeftShapeLayer.path = UIBezierPath(arcCenter: CGPoint(x: view.frame.midX , y: height), radius:
            50, startAngle: -90.degreesToRadians, endAngle: 270.degreesToRadians, clockwise: true).cgPath
        timeLeftShapeLayer.strokeColor = UIColor.red.cgColor
        timeLeftShapeLayer.fillColor = UIColor.clear.cgColor
        timeLeftShapeLayer.lineWidth = 5
        let bgView = UIView(frame: timeLeftShapeLayer.frame)
        bgView.borderWidth1 = 5
        bgView.borderColor1 = .red
        self.view.addSubview(bgView)
        self.view.layer.addSublayer(timeLeftShapeLayer)
    }
    
    func addTimeLabel() {
        let height = UIScreen.main.bounds.size.height - 225
        timeLabel = UILabel(frame: CGRect(x: view.frame.midX-50 ,y: height-25, width: 100, height: 50))
        timeLabel.textColor = .white
        timeLabel.textAlignment = .center
        timeLabel.text = timeLeft.time
        self.view.addSubview(timeLabel)
    }

    
    @objc func updateTime() {
        if timeLeft > 0 {
            timeLeft = endTime?.timeIntervalSinceNow ?? 0
            timeLabel.text = timeLeft.time
        }
        else {
            timeLabel.text = "00:00"
            if let walkerId = objCurrentReqest?.walker_id, let memberId = objCurrentReqest?.ownerId, let date = objCurrentReqest?.pickup_date {
                let role = SharedClass.sharedInstance.getUserRoleNo()
                self.objWalkViewModel?.performEndWalk(body: ["walker_id":walkerId, "member_id":memberId, "date":date, "time":"30", "role":role, "status":"1", "route_picture":"1"], controller: self)
            }
            timer.invalidate()

            guard let ownerId = objCurrentReqest?.ownerId else {
                return
            }
            self.removeLocation(withID: "Locations/WalkingLocation/dogowner_\(ownerId)")
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay === self.accuracyRangeCircle{
            let circleRenderer = MKCircleRenderer(circle: overlay as! MKCircle)
            circleRenderer.fillColor = UIColor(white: 0.0, alpha: 0.25)
            circleRenderer.lineWidth = 0
            return circleRenderer
        }else{
            let polylineRenderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
            polylineRenderer.strokeColor = UIColor(hex:0x1b60fe)
            polylineRenderer.alpha = 0.5
            polylineRenderer.lineWidth = 5.0
            return polylineRenderer
        }
    }
    
    func updatePolylines(locations: [CLLocation]) {
        var coordinateArray = [CLLocationCoordinate2D]()
        
        for loc in locations {
            coordinateArray.append(loc.coordinate)
        }
        self.clearPolyline()
        self.polyline = MKPolyline(coordinates: coordinateArray, count: coordinateArray.count)
        self.mapView.addOverlay(polyline!)
    }
    
    func clearPolyline() {
        if self.polyline != nil{
            self.mapView.removeOverlay(self.polyline!)
            self.polyline = nil
        }
        else {
            self.mapView.overlays.forEach {
                if !($0 is MKUserLocation) {
                    self.mapView.removeOverlay($0)
                }
            }        }
    }
    
    func zoomTo(location: CLLocation) {
        if self.didInitialZoom == false{
            let coordinate = location.coordinate
            let region = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: 300, longitudinalMeters: 300)
            self.mapView.setRegion(region, animated: false)
            self.didInitialZoom = true
        }
        
        if self.isBlockingAutoZoom == false{
            self.isZooming = true
            self.mapView.setCenter(location.coordinate, animated: true)
        }
        
        var accuracyRadius = 50.0
        if location.horizontalAccuracy > 0{
            if location.horizontalAccuracy > accuracyRadius{
                accuracyRadius = location.horizontalAccuracy
            }
        }
        
        if let rangeCircle = self.accuracyRangeCircle {
            self.mapView.removeOverlay(rangeCircle)
        }
        self.accuracyRangeCircle = MKCircle(center: location.coordinate, radius: accuracyRadius as CLLocationDistance)
        if let rangeCircle = self.accuracyRangeCircle {
            self.mapView.addOverlay(rangeCircle)
        }
        
        if self.userAnnotation != nil{
            self.mapView.removeAnnotation(self.userAnnotation!)
        }
        
        self.userAnnotation = UserAnnotation(coordinate: location.coordinate, title: "", subtitle: "")
        self.mapView.addAnnotation(self.userAnnotation!)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil
        }else{
            let identifier = "UserAnnotation"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView != nil{
                annotationView!.annotation = annotation
            }else{
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            annotationView!.canShowCallout = false
            annotationView!.image = self.userAnnotationImage
            
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        if self.isZooming == true{
            self.isZooming = false
            self.isBlockingAutoZoom = false
        }else{
            self.isBlockingAutoZoom = true
            if let timer = self.zoomBlockingTimer{
                if timer.isValid{
                    timer.invalidate()
                }
            }
            self.zoomBlockingTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false, block: { (Timer) in
                self.zoomBlockingTimer = nil
                self.isBlockingAutoZoom = false;
            })
        }
    }
    
    public func removeLocation(withID: String) {
        let ref = Database.database().reference(withPath: withID)
            ref.removeValue { error, _ in
            print(error?.localizedDescription ?? "")
          }
    }
    
}

extension TimeInterval {
    var time: String {
        return String(format:"%02d:%02d", Int(self/60),  Int(ceil(truncatingRemainder(dividingBy: 60))) )
    }
}

extension Int {
    var degreesToRadians : CGFloat {
        return CGFloat(self) * .pi / 180
    }
}
