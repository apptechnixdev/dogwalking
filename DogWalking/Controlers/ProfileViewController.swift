//
//  ProfileViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/09/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage
import Firebase

protocol PetsView {
func setPetLists(pets: [Pet])
}
protocol WalkersView {
func setWalkerNewRequestResults(requests:[WalkerRequest])
func setWalkerInProgressRequestResults(requests:[WalkerRequest])
func newRequestUpdated()
}
class PetCell: UITableViewCell {
    @IBOutlet weak var imgPetProfile: UIImageView!
    @IBOutlet weak var imgDetailArrow: UIImageView!
    @IBOutlet weak var lblPetName: UILabel!
    @IBOutlet weak var lblPetDate: UILabel!
    @IBOutlet weak var btnBookWalk: UIButton!
}

class ProfileViewController: UIViewController {
    @IBOutlet weak var imgOwnerProfile: UIImageView!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var btnOwnerStatus: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewRequest: UIView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var lblRequestDetail: UILabel!

    var aryPets: [Pet] = []
    var aryRequests: [WalkerRequest] = []
    var objPetViewModel: PetViewModel?
    var objWalkerViewModel: WalkerViewModel?
    var objWalkViewModel: WalkViewModel?
    var objPet: Pet?
    var objCurrentReqest: WalkerRequest?


    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview.tableFooterView = UIView()
        self.objWalkViewModel = WalkViewModel()
        if SharedClass.sharedInstance.getUserRole() == .dogOwner{
            self.configureNavigationBar(largeTitleColor: .black, backgoundColor: .white, tintColor: .white, title: "Profile", preferredLargeTitle: false)
            self.objPetViewModel = PetViewModel(view: self)
        }
        else {
            self.configureNavigationBar(largeTitleColor: .black, backgoundColor: .white, tintColor: .white, title: "Dog Walker", preferredLargeTitle: false)
            self.objWalkerViewModel = WalkerViewModel(view: self)
        }
        self.configureUserDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if SharedClass.sharedInstance.getUserRole() == .dogWalker{
            self.loadWalkerRequest()
        }
        else {
            guard let ownerId = UserManager.sharedInstance.getUserDetials().user_id else {
                return        }
            self.objPetViewModel?.getPetLists(id: ownerId, controller: self)
            self.checkOnGoingWalk()
        }
        self.setLeftBarItemProfileImage(self, #selector(onBtnLeftBarItemTapped), imageName:"settings-2")
        

        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    func configureUserDetails() {
        var userName = ""
        if let fname = UserManager.sharedInstance.getUserDetials().firstname {
            userName = fname
        }
        if let lname = UserManager.sharedInstance.getUserDetials().surname {
            userName += " \(lname)"
        }
        self.lblOwnerName.text = userName
        if let address = UserManager.sharedInstance.getUserDetials().address {
            self.btnOwnerStatus.setTitle(address, for: .normal)
        }
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: UserManager.sharedInstance.getUserDetials().profile_picture ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.imgOwnerProfile.bounds.size,
                radius: self.imgOwnerProfile.bounds.size.width / 2
            )
            self.imgOwnerProfile.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgOwnerProfile?.image = placeholderImage
        }
    }
    
    func configurePetDetails(){
        self.aryPets.removeAll()
        self.aryPets = PetManger.sharedInstance.getAllPets()
        self.tableview.reloadData()
    }
    
    @objc func onBtnLeftBarItemTapped() {
        SharedClass.sharedInstance.getSideMenuController().toggleLeftMenu()
    }
    
    @objc func trackTapped() {
        LocationService.sharedInstance.startUpdatingLocation()
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objThankYouViewController = storyBoard.instantiateViewController(withIdentifier: "StartWalkViewController") as! StartWalkViewController
        self.navigationController?.pushViewController(objThankYouViewController, animated: true)
    }
    
    @objc func onBtnStartWalkTapped(sender: UIButton) {
        self.objCurrentReqest = aryRequests[sender.tag]
        LocationService.sharedInstance.startUpdatingLocation()
        if let walkerId = objCurrentReqest?.walker_id, let memberId = objCurrentReqest?.ownerId, let date = objCurrentReqest?.pickup_date {
            let role = SharedClass.sharedInstance.getUserRoleNo()
            self.objWalkViewModel?.performStartWalk(body: ["walker_id":walkerId, "member_id":memberId, "date":date, "time":"30", "role":role], objCurrentReqest: objCurrentReqest, controller: self)
        }
    }
    
    @objc func onBtnBookWalkTapped(sender: UIButton) {
        let objPet = self.aryPets[sender.tag]
        let objDogProfileViewController = storyboard!.instantiateViewController(withIdentifier: "DogProfileViewController") as! DogProfileViewController
            objDogProfileViewController.objPet = objPet
        self.navigationController?.pushViewController(objDogProfileViewController, animated: true)
    }
    
    func checkOnGoingWalk() {
        guard let user_id = UserManager.sharedInstance.getUserDetials().user_id else {
            return
        }
        let ref = Database.database().reference(withPath: "Locations/WalkingLocation/dogowner_\(user_id)")
        self.navigationItem.rightBarButtonItem = nil
        ref.queryOrdered(byChild: "timeStamp").observe(.value, with: { snapshot in
            for _ in snapshot.children {
                let track = UIBarButtonItem(title: "Track", style: .plain, target: self, action: #selector(self.trackTapped))
                track.setTitleTextAttributes([.foregroundColor: UIColor.red], for: .normal)
                self.navigationItem.rightBarButtonItem = track
                break
            }
        })
    }
    
    func loadWalkerRequest() {
        self.viewContainer.isHidden = false
        self.viewRequest.removeFromSuperview()
        if WalkerRequestsManager.sharedInstance.getWalkerRequests().count > 0 {
            self.viewContainer.isHidden = true
            self.view.addSubview(self.viewRequest)
            self.viewRequest.translatesAutoresizingMaskIntoConstraints = false
            self.viewRequest.leadingAnchor.constraint(equalTo: self.viewContainer.leadingAnchor, constant: 0.0).isActive = true
            self.viewRequest.trailingAnchor.constraint(equalTo: self.viewContainer.trailingAnchor, constant: 0.0).isActive = true
            self.viewRequest.topAnchor.constraint(equalTo: self.viewContainer.topAnchor, constant: 0   ).isActive = true
            self.viewRequest.bottomAnchor.constraint(equalTo: self.viewContainer.bottomAnchor, constant: 0.0).isActive = true
            self.loadNewRequestDetails()
        }
        else {
            self.objWalkerViewModel?.getWalkerInProgressRequest(controller:self)
        }
    }
    
    func updateWalkerNewRequest(status: String) {
        if let user_id = UserManager.sharedInstance.getUserDetials().user_id, let requestId = objCurrentReqest?.request_id {
            let body = ["request_id": requestId, "status_id":status, "walker_id":user_id]
            self.objWalkerViewModel?.updateWalkerNewRequest(params: body, controller: self)
        }
    }
    
    @IBAction func onBtnAcceptTapped(sender: Any) {
        self.updateWalkerNewRequest(status: "3")
    }
    
    @IBAction func onBtnDeclineTapped(sender: Any) {
        self.updateWalkerNewRequest(status: "6")
    }

    func loadNewRequestDetails() {
        objCurrentReqest = WalkerRequestsManager.sharedInstance.getWalkerRequests().first
        var sDate = ""
        if let date = Date.FromString(objCurrentReqest?.pickup_date ?? "") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
           sDate = dateFormatter.string(from: date)
        }
        lblRequestDetail.text = "\(objCurrentReqest?.address ?? "") \n\(sDate)"
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SharedClass.sharedInstance.getUserRole() == .dogOwner{
        return aryPets.count
        }
            return aryRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PetCell", for: indexPath) as! PetCell
        if SharedClass.sharedInstance.getUserRole() == .dogOwner{
            cell.lblPetDate.isHidden = true
        let objPet = aryPets[indexPath.row]
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: objPet.pet_image ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: cell.imgPetProfile.bounds.size,
                radius: cell.imgPetProfile.bounds.size.width / 2
            )
            cell.imgPetProfile.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            cell.imgPetProfile?.image = placeholderImage
        }
        if let fname = objPet.name {
            cell.lblPetName.text = fname
        }
            cell.btnBookWalk.setTitle("Book Walk", for: .normal)
        cell.btnBookWalk.addTarget(self, action: #selector(onBtnBookWalkTapped(sender:)), for: .touchUpInside)
        cell.btnBookWalk.tag = indexPath.row
        }
        else{
            cell.imgDetailArrow.isHidden = true
            let objrequest = aryRequests[indexPath.row]
            let placeholderImage = UIImage(named: "DogMask")!
            if let imgUrl = URL(string: objrequest.pet_image ?? "") {
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.imgPetProfile.bounds.size,
                    radius: cell.imgPetProfile.bounds.size.width / 2
                )
                cell.imgPetProfile.af.setImage(
                    withURL: imgUrl,
                    placeholderImage: placeholderImage,
                    filter: filter,
                    imageTransition: .crossDissolve(0.2)
                )
            }
            else{
                cell.imgPetProfile?.image = placeholderImage
            }
            if let fname = objrequest.pet_name {
                cell.lblPetName.text = fname
            }
            if let date = objrequest.pickup_date,  let time = date.components(separatedBy: " ").last {
                cell.lblPetDate.text = time
            }
            cell.btnBookWalk.setTitle("Start Walk", for: .normal)
            cell.btnBookWalk.addTarget(self, action: #selector(onBtnStartWalkTapped(sender:)), for: .touchUpInside)
            cell.btnBookWalk.tag = indexPath.row

        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if SharedClass.sharedInstance.getUserRole() == .dogOwner{
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objUpdatePetViewController = storyBoard.instantiateViewController(withIdentifier: "UpdatePetViewController") as! UpdatePetViewController
        objUpdatePetViewController.objPet = aryPets[indexPath.row]
        self.navigationController?.pushViewController(objUpdatePetViewController, animated: true)
        }
    }
}


extension ProfileViewController: PetsView {
    func setPetLists(pets: [Pet]) {
        self.aryPets = pets
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }

    }
    
}
extension ProfileViewController: WalkersView  {
    func setWalkerNewRequestResults(requests: [WalkerRequest]) {

    }
    
    func setWalkerInProgressRequestResults(requests: [WalkerRequest]) {
        self.aryRequests = requests
        DispatchQueue.main.async {
            self.tableview.reloadData()
        }
    }
    
    func newRequestUpdated() {
        var requests = WalkerRequestsManager.sharedInstance.getWalkerRequests()
        if let index = requests.firstIndex(where: {$0.request_id == objCurrentReqest?.request_id}) {
            requests.remove(at: index)
        }
        WalkerRequestsManager.sharedInstance.storeWalkerRequests(requests: requests)
        self.loadWalkerRequest()
    }

    }
    


