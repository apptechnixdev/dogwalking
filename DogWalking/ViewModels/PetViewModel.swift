//
//  PetViewModel.swift
//  DogWalking
//
//  Created by Admin on 2020/09/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PetViewModel: NSObject {
    private var view : PetsView?
    private let objPetRepository = PetRepositoryImpl()

    init(view: PetsView?) {
        self.view = view
    }

    
    func performAddPet(body:[String:String], controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objPetRepository.performAddPet(params: body) { (error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Success", message: "Pet added successfully.", action: AlertAction(buttonTitle: "OK", handler: {
                    let navigation = SharedClass.sharedInstance.getSideMenuController().contentController as? UINavigationController
                    navigation?.popToRootViewController(animated: true)
                })) )

            }
        }
    }
    func performUpdatePet(body:[String:String], controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objPetRepository.performUpdatePet(params: body) { (error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if let ownerId = UserManager.sharedInstance.getUserDetials().user_id  {
                    self.getPetLists(id: ownerId, controller: controller)
                    }
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Success", message: "Pet updated successfully.", action: AlertAction(buttonTitle: "OK", handler: {
                    let navigation = SharedClass.sharedInstance.getSideMenuController().contentController as? UINavigationController
                    navigation?.popToRootViewController(animated: true)
                })) )

            }
        }
    }

    func getPetLists(id: String, controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objPetRepository.getPetLists(id: id, completion: { (pets, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if let pets = pets {
                    self.view?.setPetLists(pets: pets)
                }
            }
        })
    }



}
