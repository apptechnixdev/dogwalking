//
//  FeedRepositoryImpl.swift
//  recyclego-ios
//
//  Created by Admin on 2020/08/06.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class FeedRepositoryImpl: NSObject {

}
extension FeedRepositoryImpl: FeedRepository {
    
    func getFeedLikes(feedId: String, completion: @escaping (([Likes]?, Error?) -> Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/newsfeed/read_likes.php?newsfeed_id=\(feedId)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[Likes],Error>) in
            switch result{
            case .success(let likes):
                completion(likes, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }
    }
    
    func getFeedList(completion: @escaping (([Feed]?, Error?) -> Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }

        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/newsfeed/read_newsfeed.php", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[Feed],Error>) in
            switch result{
            case .success(let feeds):
                completion(feeds, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }

    }
    
    func getFeedComments(feedId:String,memberId:String,completion:@escaping (([Comments]?, Error?)->Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/newsfeed/read_comments.php?newsfeed_id=\(feedId)&member_id=\(memberId)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[Comments],Error>) in
            switch result{
            case .success(let comments):
                completion(comments, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }
    }
    
    
    
    func performAddCooment(params: [String:String], completion:@escaping ((Bool, Error?)->Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(data: params, headers:["Authorization":"Bearer \(token)"], url: "apis/v1/newsfeed/create_comments.php", service: .posts, method: .post)
        objNetwork.executePostQuery() {
            (result: Result<[String:String]?,Error>) in
            switch result{
            case .success( _):
                completion(true, nil)
            case .failure(let error1):
                completion(false, error1)
            }
        }
    }
    
    func performAddLike(params: [String:String], completion:@escaping ((Bool, Error?)->Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(data: params, headers:["Authorization":"Bearer \(token)"], url: "apis/v1/newsfeed/create_likes.php", service: .posts, method: .post)
        objNetwork.executePostQuery() {
            (result: Result<[String:String]?,Error>) in
            switch result{
            case .success( _):
                completion(true, nil)
            case .failure(let error1):
                completion(false, error1)
            }
        }

    }
}
    

