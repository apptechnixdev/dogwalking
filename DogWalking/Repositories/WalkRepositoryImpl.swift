//
//  WalkRepositoryImpl.swift
//  DogWalking
//
//  Created by Admin on 2021/01/04.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

class WalkRepositoryImpl: NSObject {

}
extension WalkRepositoryImpl: WalkRepository {
    func getWalkHistory(userId: String, completion: @escaping (([WalkHistory]?, Error?) -> Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/walk_history.php?member_id=\(userId)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[WalkHistory],Error>) in
            switch result{
            case .success(let history):
                completion(history, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }

    }
    
    func performStartWalk(params: [String : Any], completion: @escaping ((Error?) -> Void)) {
        let objNetwork = NetworkCall(data: params, url: "apis/v1/start_walk.php", service: .posts, method: .post)
        objNetwork.executeNonResultQuery() {
            (result: EmptyResult<Error>) in
            switch result{
            case .success:
                completion(nil)
            case .failure(let error1):
                completion(error1)
            }
        }
    }
    
    func performEndWalk(params: [String : Any], completion: @escaping ((Error?) -> Void)) {
        let objNetwork = NetworkCall(data: params, url: "apis/v1/end_walk.php", service: .posts, method: .post)
        objNetwork.executeNonResultQuery() {
            (result: EmptyResult<Error>) in
            switch result{
            case .success:
                completion(nil)
            case .failure(let error1):
                completion(error1)
            }
        }

    }

}
    

