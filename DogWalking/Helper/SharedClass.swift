//
//  SharedClass.swift
//
//  Created by Admin on 2020/06/07.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import JGProgressHUD
import Firebase
enum RoleType :String{
    case dogWalker
    case dogOwner
}

class SharedClass: NSObject {
    
    static let sharedInstance = SharedClass()
    let loader = JGProgressHUD(style: .extraLight)
    var cloudUrl: String? = nil

    func showLoader(_ strTitle:String = "Loading") {
        DispatchQueue.main.async {
            if var topController = UIApplication.shared.delegate?.window??.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                self.loader.textLabel.text = strTitle
                self.loader.show(in: topController.view)
            }
        }
    }
    func getUserRole() -> RoleType{
        let role = UserManager.sharedInstance.getUserDetials().role
        switch role {
        case "dog_walker":
            return .dogWalker
        case "dog_owner":
            return .dogOwner
        case .none, .some(_): break
        }
        return RoleType(rawValue: role!) ?? .dogOwner
    }
    
    func getUserRoleNo() -> Int {
        let roleType = getUserRole()
        if roleType == .dogWalker {
            return 1
        }
        else if roleType == .dogOwner {
            return 2
        }
        return 1
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            self.loader.dismiss(animated: true)
        }
    }
    
    func getSideMenuController() -> PGSideMenu{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window?.rootViewController as! PGSideMenu
    }

    func getApplicationDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate

    }
    
    func getApplicationWindow() -> UIWindow? {
        let appDelegate = getApplicationDelegate()
        appDelegate.window?.makeKeyAndVisible()
        return appDelegate.window
    }
    
    func logOutApp() {
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objLoginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        SharedClass.sharedInstance.getApplicationWindow()?.rootViewController = getNavigationController(root: objLoginViewController)
    }
    
    func getNavigationController(root: UIViewController, title: String = "")->UINavigationController{
        let navigationController = UINavigationController(rootViewController: root)
        navigationController.title = title
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.barStyle = .black
        navigationController.navigationBar.barTintColor = .white
        return navigationController
    }

    
    func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())

        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }

    }
    func uploadImagePic(image: UIImage, name: String, filePath: String, _ callback: @escaping ((Bool) -> ())) {
        guard let imageData: Data = image.jpegData(compressionQuality: 0.1) else {
            return
        }
         cloudUrl = nil
        let metaDataConfig = StorageMetadata()
        metaDataConfig.contentType = "image/jpeg"

        let storageRef = Storage.storage().reference(withPath: "dogWalkingUser/\(name)")

        storageRef.putData(imageData, metadata: metaDataConfig){ (metaData, error) in
            if let error = error {
                print(error.localizedDescription)
                callback(false)
                return
            }
            storageRef.downloadURL(completion: { (url: URL?, error: Error?) in
                self.cloudUrl = url?.absoluteString
                callback(true)
            })
        }
    }

    
    func getContentDateView(pickUpDate: String, _ target: UIViewController?, _ selector: Selector?, index: Int, _ isOnlyDate :Bool = false) -> UIView? {
        let date = pickUpDate.getDateFromFormatter(formatter: "yyyy-MM-dd HH:mm:ss")
        let dateFormatter = DateFormatter()
            var text : NSAttributedString?
            if isOnlyDate {
                dateFormatter.dateFormat = "dd MMM"
                let sDate = dateFormatter.string(from: date)
                let component = sDate.components(separatedBy: " ")
                
                guard component.count == 2, let day = component.first as NSString?, let month = component[1] as? NSString, let normalFont = UIFont(name: "Lato-Semibold", size: 12), let boldSearchFont = UIFont(name: "Lato-Heavy", size: 16) else {
                    return nil
                }
                let newText = "\(day) \n\(month)"
                text = newText.addBoldText(boldPartOfString: day, font: normalFont, boldFont: boldSearchFont)
            }
            else {
                dateFormatter.dateFormat = "dd MMM HH:mm"
                let sDate = dateFormatter.string(from: date)
                let component = sDate.components(separatedBy: " ")
                
                guard component.count == 3, let day = component.first as NSString?, let month = component[1] as? NSString, let time = component.last as NSString?, let normalFont = UIFont(name: "Lato-Semibold", size: 12), let boldSearchFont = UIFont(name: "Lato-Heavy", size: 16) else {
                    return nil
                }
                let newText = "\(day) \(month) \n\(time)"
                text = newText.addBoldText(boldPartOfString: day, font: normalFont, boldFont: boldSearchFont)
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 49))
            let button = UIButton(type: .custom)
            if let target = target, let selector = selector {
                button.addTarget(target, action: selector, for: .touchUpInside)
            }
            button.contentHorizontalAlignment = .center
            button.setAttributedTitle(text, for: .normal)
            button.setTitleColor(UIColor(hex: 0x202224), for: .normal)
            button.backgroundColor = UIColor(hex: 0xF1F2F6)
            button.titleLabel?.numberOfLines = 0
            button.titleLabel?.lineBreakMode = .byWordWrapping
            button.titleLabel?.textAlignment = .center
            button.titleLabel?.textColor = UIColor(hex: 0x202224)
            button.frame = view.frame
            button.tag = index
            view.sizeToFit()
            view.backgroundColor = UIColor(hex: 0xF1F2F6)
            view.cornerRadius1 = 5
            button.cornerRadius1 = 5

            view.borderWidth1 = 1
            view.borderColor1 = UIColor(hex: 0xD1D1D1)
            view.addSubview(button)
            return view
    }
    
    func getUserName() -> String {
        var userName = ""
        if let fname = UserManager.sharedInstance.getUserDetials().firstname {
            userName = fname
        }
        if let lname = UserManager.sharedInstance.getUserDetials().surname {
            userName += " \(lname)"
        }
        return userName
    }
}

