//
//  ScheduleRepositoryImpl.swift
//  DogWalking
//
//  Created by Admin on 2020/10/16.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ScheduleRepositoryImpl: NSObject {

}
extension ScheduleRepositoryImpl: ScheduleRepository {
    
    func getWalkerNewRequest(userId: String, completion: @escaping (([WalkerRequest]?, Error?) -> Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/newsfeed/read_walkernewrequests.php?walker_id=\(userId)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[WalkerRequest],Error>) in
            switch result{
            case .success(let requests):
                WalkerRequestsManager.sharedInstance.storeWalkerRequests(requests: requests)
                completion(requests, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }
    }
    
    func getWalkerInProgressRequest(userId: String, completion: @escaping (([WalkerRequest]?, Error?) -> Void)) {
        guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/newsfeed/read_walkerIPrequests.php?walker_id=\(userId)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[WalkerRequest],Error>) in
            switch result{
            case .success(let requests):
                completion(requests, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }
    }
    
    func updateWalkerNewRequest(params: [String : String], completion: @escaping ((Bool, Error?) -> Void)) {
            guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
                return
            }
            let objNetwork = NetworkCall(data: params, headers:["Authorization":"Bearer \(token)"], url: "apis/v1/newsfeed/update_walkernewreqpickup.php", service: .posts, method: .post)
            objNetwork.executePostQuery() {
                (result: Result<[String:String]?,Error>) in
                switch result{
                case .success( _):
                    completion(true, nil)
                case .failure(let error1):
                    completion(false, error1)
                }
            }
        }
    
    func performScheduleWalk(params: [String : Any], completion: @escaping ((Error?) -> Void)) {
            let objNetwork = NetworkCall(data: params, url: "apis/v1/pet/request_walk.php", service: .posts, method: .post)
            objNetwork.executeNonResultQuery() {
                (result: EmptyResult<Error>) in
                switch result{
                case .success:
                    completion(nil)
                case .failure(let error1):
                    completion(error1)
                }
            }

        }

    }
    

