//
//  LoginViewController.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet weak var txtPassword: DTTextField! {
        didSet{
            txtPassword.rightViewMode = .always
            txtPassword.rightView = txtPassword.getHideShowView(target: self, selector: #selector(onBtnPasswordHideShow))
        }
    }
   // @IBOutlet weak var btnSignIn: UIButton!
    private let viewModel = LoginViewModel()
    public var strEmail: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setLeftBarItemProfileImage()
        self.navigationItem.hidesBackButton = true

        if let email = strEmail {
            txtEmail.text = email
        }
        else if let email = UDWrapper.getString(key: "isLogin") as NSString? {
            txtEmail.text = email as String
        }

//        txtEmail.text = "dogwalker@test.com"
       // txtPassword.text = "test123"

        self.setUpTextFields()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.configureDefaultNavigationBar(title: "Login")
    }
        
    func setUpTextFields() {
        txtEmail.addPadding(padding: .left(13))
        txtPassword.addPadding(padding: .left(13))
        //txtEmail.text = "dogowner@gmail.com"
        //txtPassword.text = "test123"
       // self.onBtnTappedSignIn(sender: UIButton())
    }

    func validateLoginFields() -> Bool {
        guard !txtEmail.text!.isEmptyStr
            else {
            txtEmail.showError(message: ValidationText.emailMessage)
            return false
        }
        guard txtEmail.isValidEmail(txtEmail.text ?? "") else {
            txtEmail.showError(message: ValidationText.emailMessage1)
            return false
        }

        guard !txtPassword.text!.isEmptyStr else {
            txtPassword.showError(message: ValidationText.passwordMessage)
            return false
        }
        guard !txtPassword.text!.isUpper else {
            txtPassword.showError(message: ValidationText.caseSensitivePassword)
            return false
        }
                    return true
    }

    @IBAction func onBtnLoginTapped(sender: Any) {
        if validateLoginFields(){
            if let email = txtEmail.text, let password = txtPassword.text {
                viewModel.performLogin(body: ["email":email, "password":password], controller: self)
            }
        }
     }
    
    @objc func onBtnPasswordHideShow(sender: UIButton) {
        sender.isSelected  = !sender.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
}
