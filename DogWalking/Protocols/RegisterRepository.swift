//
//  RegisterRepository.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol RegisterRepository {
    func performRegister(params: [String:String], completion:@escaping ((Error?)->Void))
    func performUpdateUserDetails(params: [String:String], completion:@escaping ((UserDetails?, Error?)->Void))

}
