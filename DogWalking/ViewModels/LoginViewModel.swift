//
//  LoginViewModel.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class LoginViewModel: NSObject {
    private let objLoginRepository = LoginRepositoryImpl()
    
    func performLogin(body:[String:String], controller : UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objLoginRepository.performLogin(params: body) { (user, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                UDWrapper.setString(key: "isLogin", value: body["email"] as NSString?)
                let objWalkerViewModel = WalkerViewModel(view: nil)
                if SharedClass.sharedInstance.getUserRole() == .dogWalker {
                    objWalkerViewModel.getWalkerNewRequest(controller: controller) {
                        self.loadAppStructure()
                    }
                }
                else {
                    self.loadAppStructure()
                }
            }
        }
    }
    
    func loadAppStructure() {
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objSideMenuViewControler = storyBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController

        let pgSideMenuController = PGSideMenu(animationType: .slideIn)
        if SharedClass.sharedInstance.getUserRole() == .dogOwner {
            let objBaseViewControler = storyBoard.instantiateViewController(withIdentifier: "baseNavigation") as! UINavigationController
            pgSideMenuController.addContentController(objBaseViewControler)
        }
        else if SharedClass.sharedInstance.getUserRole() == .dogWalker {
            let objBaseViewControler = storyBoard.instantiateViewController(withIdentifier: "baseNavigation") as! UINavigationController
            pgSideMenuController.addContentController(objBaseViewControler)
        }
        pgSideMenuController.addLeftMenuController(objSideMenuViewControler)
        SharedClass.sharedInstance.getApplicationWindow()?.rootViewController = pgSideMenuController
    }

}
