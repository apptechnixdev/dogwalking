//
//  SideMenuViewController.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/24.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class SideMenuCell: UITableViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}

class SideMenuViewController: UIViewController {
    @IBOutlet weak var tblDetail: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    var sideMenuItems: [[String : String]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setUserDetails()
        if SharedClass.sharedInstance.getUserRole() == .dogOwner {
           sideMenuItems = [["icon":"Home-2","text": "HOME"], ["icon":"QR Code","text": "SCHEDULE"], ["icon":"Add a pet","text": "ADD A PET"],["icon":"Add a pet","text": "NOTIFICATIONS"],["icon":"favorites icon","text": "ABOUT A HAPPY PAW!"],["icon":"settings-1","text": "ACCOUNT SETTINGS"], ["icon":"Logout","text": "LOG OUT"]]
        }
        else {
            sideMenuItems = [["icon":"Home-2","text": "HOME"],["icon":"Add a pet","text": "NOTIFICATIONS"],["icon":"favorites icon","text": "ABOUT A HAPPY PAW!"],["icon":"settings-1","text": "ACCOUNT SETTINGS"], ["icon":"Logout","text": "LOG OUT"]]

        }
    }
    
    func setUserDetails() {
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: UserManager.sharedInstance.getUserDetials().profile_picture ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.imgProfile.bounds.size,
                radius: self.imgProfile.bounds.size.width / 2
            )
            self.imgProfile.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgProfile?.image = placeholderImage
        }
        lblProfileName.text = SharedClass.sharedInstance.getUserName()
        
        if let address = UserManager.sharedInstance.getUserDetials().address {
            if address.components(separatedBy: " ").last != nil {
                //lblArea.text = area
            }
        }
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
        let obj = sideMenuItems[indexPath.row]
        cell.imgIcon.image = UIImage.init(named: obj["icon"]!)
        cell.lblName.text = obj["text"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navigation = SharedClass.sharedInstance.getSideMenuController().contentController as? UINavigationController
        SharedClass.sharedInstance.getSideMenuController().toggleLeftMenu()
        if indexPath.row == 0 {
            let objProfileViewController = storyboard!.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                navigation?.pushViewController(objProfileViewController, animated: true)
        }
       else if indexPath.row == 1 {
            let objScheduleViewController = storyboard!.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
                navigation?.pushViewController(objScheduleViewController, animated: true)
        }
        else if (SharedClass.sharedInstance.getUserRole() == .dogOwner
                    && indexPath.row == 2) {
             let objAddPetViewController = storyboard!.instantiateViewController(withIdentifier: "AddPetViewController") as! AddPetViewController
                 navigation?.pushViewController(objAddPetViewController, animated: true)
         }
        else if (SharedClass.sharedInstance.getUserRole() == .dogOwner
            && indexPath.row == 4) || (SharedClass.sharedInstance.getUserRole() == .dogWalker && indexPath.row == 2){
                  let objAboutDogWalkingViewController = storyboard!.instantiateViewController(withIdentifier: "AboutDogWalkingViewController") as! AboutDogWalkingViewController
                      navigation?.pushViewController(objAboutDogWalkingViewController, animated: true)
                  }

        
          else if (SharedClass.sharedInstance.getUserRole() == .dogOwner
              && indexPath.row == 5) || (SharedClass.sharedInstance.getUserRole() == .dogWalker && indexPath.row == 3){
                    let objAccountSettingsViewController = storyboard!.instantiateViewController(withIdentifier: "AccountSettingsViewController") as! AccountSettingsViewController
                        navigation?.pushViewController(objAccountSettingsViewController, animated: true)
                    }

        else if (SharedClass.sharedInstance.getUserRole() == .dogOwner
            && indexPath.row == 6) || (SharedClass.sharedInstance.getUserRole() == .dogWalker && indexPath.row == 4){
                 SharedClass.sharedInstance.logOutApp()
        }
    }
}

