//
//  PetRepositoryImpl.swift
//  DogWalking
//
//  Created by Admin on 2020/09/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PetRepositoryImpl: NSObject {

}
extension PetRepositoryImpl: PetRepository {
    
    func performUpdatePet(params: [String : String], completion: @escaping ((Error?) -> Void)) {
            let objNetwork = NetworkCall(data: params, url: "apis/v1/pet/update_pet.php", service: .posts, method: .post)
            objNetwork.executeNonResultQuery() {
                (result: EmptyResult<Error>) in
                switch result{
                case .success:
                    completion(nil)
                case .failure(let error1):
                    completion(error1)
                }
            }

        }

    func getPetLists(id: String, completion: @escaping (([Pet]?, Error?) -> Void)) {
            guard  let token = UserManager.sharedInstance.getUserDetials().access_token else {
            return
        }
        let objNetwork = NetworkCall(headers:["Authorization":"Bearer \(token)"] ,customUrl: "apis/v1/pet/list_pets.php?id=\(id)", service: .gets, method: .get)
        objNetwork.executeGetReqest() {
            (result: Result<[Pet],Error>) in
            switch result{
            case .success(let pets):
                PetManger.sharedInstance.storePetsDetail(pet: pets)
                completion(pets, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }

    }
    
    func performAddPet(params: [String : String], completion: @escaping ((Error?) -> Void)) {
            let objNetwork = NetworkCall(data: params, url: "apis/v1/pet/add_pet.php", service: .posts, method: .post)
            objNetwork.executeNonResultQuery() {
                (result: EmptyResult<Error>) in
                switch result{
                case .success:
                    completion(nil)
                case .failure(let error1):
                    completion(error1)
                }
            }

        }

    }


