//
//  AccountSettingsViewModel.swift
//  DogWalking
//
//  Created by Admin on 2020/10/13.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AccountSettingsViewModel: NSObject {
    private let objRegisterRepository = RegisterRepositoryImpl()
    
    func performUpdateUserDetails(body:[String:String], controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objRegisterRepository.performUpdateUserDetails(params: body) { (user, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Success", message: "Your account details has been updated.", action: AlertAction(buttonTitle: "OK", handler: {
                    controller.navigationController?.popToRootViewController(animated: true)

                })) )

            }
        }
    }


}
