//
//  PetRepository.swift
//  DogWalking
//
//  Created by Admin on 2020/09/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol PetRepository {
    func performAddPet(params: [String:String], completion:@escaping ((Error?)->Void))
    func getPetLists(id: String, completion: @escaping (([Pet]?, Error?) -> Void))
    func performUpdatePet(params: [String:String], completion:@escaping ((Error?)->Void))

}
