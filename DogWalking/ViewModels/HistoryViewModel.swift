//
//  HistoryViewModel.swift
//  DogWalking
//
//  Created by Admin on 2021/01/29.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

class HistoryViewModel: NSObject {
    private var view : HistoryView?
    private let objWalkRepositoryImpl = WalkRepositoryImpl()
    
    init(view: HistoryView?) {
        self.view = view
    }

    func getWalkHistory(controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        guard let memberId = UserManager.sharedInstance.getUserDetials().user_id else {
            return
        }
        objWalkRepositoryImpl.getWalkHistory(userId: memberId) { (response, error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                if let response = response {
                    self.view?.setHistory(history: response)
                }
            }
            
        }
    }

}
