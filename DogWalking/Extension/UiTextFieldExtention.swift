//
//  UiTextFieldExtention.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/23.
//  Copyright © 2020 Admin. All rights reserved.
//
import Foundation
import UIKit

private var __maxLengths = [UITextField: Int]()

extension UITextField {
    enum PaddingSpace {
        case left(CGFloat)
        case right(CGFloat)
        case equalSpacing(CGFloat)
    }

    func addPadding(padding: PaddingSpace) {

        self.leftViewMode = .always
        self.layer.masksToBounds = true

        switch padding {

        case .left(let spacing):
            let leftPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = leftPaddingView
            self.leftViewMode = .always

        case .right(let spacing):
            let rightPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = rightPaddingView
            self.rightViewMode = .always

        case .equalSpacing(let spacing):
            let equalPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = equalPaddingView
            self.leftViewMode = .always
            // right
            self.rightView = equalPaddingView
            self.rightViewMode = .always
        }
    }


func isValidEmail(_ email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

    let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailPred.evaluate(with: email)
}

func getHideShowView(target: UIViewController, selector: Selector) -> UIView {
    let rightButton  = UIButton(type: .custom)
    let contentView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(50), height: CGFloat(25)))
    
    rightButton.frame = CGRect(x: CGFloat(-10), y: CGFloat(10), width: CGFloat(50), height: CGFloat(25))

    rightButton.setTitle("SHOW", for: .normal)
    rightButton.setTitle("HIDE", for: .selected)
    rightButton.addTarget(target, action: selector, for: .touchUpInside)
    rightButton.titleLabel?.font = UIFont(name: "Lato-bold", size: 12)
    let myColor = UIColor(hex: 0x202224)
    contentView.addSubview(rightButton)
    rightButton.setTitleColor(myColor, for: .normal)
    rightButton.setTitleColor(myColor, for: .selected)
    return contentView
}
        @IBInspectable var maxLength: Int {
            get {
                guard let l = __maxLengths[self] else {
                   return 150 // (global default-limit. or just, Int.max)
                }
                return l
            }
            set {
                __maxLengths[self] = newValue
                addTarget(self, action: #selector(fix), for: .editingChanged)
            }
        }
    @objc func fix() {
        if let t = self.text {
            self.text = String(t.prefix(maxLength)) }
        }

}

extension String {
    var isUpper: Bool {
        return String(self.filter { String($0) == String($0).uppercased() }) == self
    }
}

