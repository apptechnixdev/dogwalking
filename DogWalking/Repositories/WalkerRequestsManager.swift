//
//  WalkerRequestsManager.swift
//  DogWalking
//
//  Created by Admin on 2020/12/02.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class WalkerRequestsManager: NSObject {
    static let sharedInstance = WalkerRequestsManager ()
    private var objRequests : [WalkerRequest] = []
    
    func getWalkerRequests() -> [WalkerRequest] {
        return objRequests
    }
    
    func storeWalkerRequests(requests: [WalkerRequest]) {
        self.objRequests = requests
    }
}
