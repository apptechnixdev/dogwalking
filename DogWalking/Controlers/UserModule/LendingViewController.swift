//
//  LendingViewController.swift
//  DogWalking-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit


class LendingViewController: UIViewController  {
    @IBOutlet weak var imgLendingImage: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func onBtnRegisterTapped(sender: Any) {
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objRegisterViewController = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
            self.navigationController?.pushViewController(objRegisterViewController, animated: true)
    }
    
    @IBAction func onBtnLoginTapped(sender: Any) {
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        let objLoginViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(objLoginViewController, animated: true)

    }

    
}
