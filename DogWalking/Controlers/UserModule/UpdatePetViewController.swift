//
//  UpdatePetViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/10/08.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class UpdatePetViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var txtPetGender: DTTextField!
    @IBOutlet weak var txtPetName: DTTextField!
    @IBOutlet weak var txtPetAge: DTTextField!
    @IBOutlet weak var txtPetColor: DTTextField!
    @IBOutlet weak var txtNotes: DTTextField!
    @IBOutlet weak var imgHalfDog: UIImageView!
    @IBOutlet weak var imgDog: UIImageView!
    var objPet: Pet?
    private var profilePicName: String = ""
    private var profilePicImage: UIImage?

    private var viewModel: PetViewModel?
    var aryGender = ["Male","Female"]
    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLeftBarItemProfileImage()
       // self.txtPetGender.inputView = self.pickerView
        self.setUpTextFields()
        self.addProfileImgGesture()
        self.viewModel = PetViewModel(view: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.configureDefaultNavigationBar(title: "Update Pet")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadPetsDetail()
    }
    func addProfileImgGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgDog.addGestureRecognizer(tapGestureRecognizer)
        imgDog.isUserInteractionEnabled = true;
    }
    
    @objc func imgTapped(_ sender:AnyObject){
        ImagePickerManager().pickImage(self){ (image, path, filename) in
            let circularImage = image.af.imageRoundedIntoCircle()
            self.imgDog.image = circularImage
            self.profilePicName = filename
            self.profilePicImage = image
        }
    }


    func setUpTextFields() {
        txtPetName.addPadding(padding: .left(13))
        txtPetAge.addPadding(padding: .left(13))
        txtNotes.addPadding(padding: .left(13))
    }
    
    func loadPetsDetail() {
        txtPetName.text = objPet?.name
        txtPetAge.text = objPet?.age
        txtNotes.text = objPet?.notes
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: objPet?.pet_image ?? "" ) {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.imgDog.frame.size,
                radius: 22
            )
            self.imgDog.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgDog?.image = placeholderImage
        }
    }
    
    func validateAddPetFields() -> Bool {
        guard !txtNotes.text!.isEmptyStr
            else {
                txtNotes.showError(message: ValidationText.petNotesMessage)
            return false
        }

        guard !txtPetName.text!.isEmptyStr else {
            txtPetName.showError(message: ValidationText.petNameMessage)
            return false
        }

        guard !txtPetAge.text!.isEmptyStr else {
            txtPetAge.showError(message: ValidationText.petAgeMessage)
            return false
        }
        guard self.imgDog.image != nil else {
            self.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: "Please add pets profile picture", action: nil) )
            return false
        }
        return true
    }
    @IBAction func onBtnBackTapped(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
     @IBAction func onBtnUpdateTapped(sender: Any) {
        if validateAddPetFields(), let petName = self.txtPetName.text, let petAge = self.txtPetAge.text, let notes = self.txtNotes.text, let owerId = UserManager.sharedInstance.getUserDetials().user_id, let petId = self.objPet?.id, let petColor = self.objPet?.color, let petGender = self.objPet?.gender
                {
                SharedClass.sharedInstance.showLoader()
            if let image = self.profilePicImage {
                DispatchQueue.global().async {
                    SharedClass.sharedInstance.uploadImagePic(image: image , name: self.profilePicName, filePath: "") { (isUploaded) in
                            
                    if isUploaded, let imageUrl = SharedClass.sharedInstance.cloudUrl {
                        self.viewModel?.performUpdatePet(body: ["name":petName, "age":petAge,"notes":notes,"owner":owerId, "pet_image": imageUrl,"id":petId, "color": petColor, "gender": petGender], controller: self)
                }
            }
        }
    }
        else {
                self.viewModel?.performUpdatePet(body: ["name":petName, "age":petAge,"notes":notes,"owner":owerId, "pet_image": objPet?.pet_image ?? "", "id":petId, "color": petColor, "gender": petGender], controller: self)
        }
    }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.aryGender.count
     }
    
     func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

         var pickerLabel = view as? UILabel;

         if (pickerLabel == nil)
         {
             pickerLabel = UILabel()

             pickerLabel?.font = UIFont(name: "Lato-Medium", size: 20)
             pickerLabel?.textAlignment = NSTextAlignment.center
         }

         pickerLabel?.text = self.aryGender[row]

         return pickerLabel!;
     }

     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtPetGender.text = self.aryGender[row]

     }



    

}
