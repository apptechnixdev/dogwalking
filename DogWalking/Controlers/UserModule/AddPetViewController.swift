//
//  AddPetViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/09/16.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class AddPetViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var txtPetGender: DTTextField!
    @IBOutlet weak var txtPetName: DTTextField!
    @IBOutlet weak var txtPetAge: DTTextField!
    @IBOutlet weak var txtPetColor: DTTextField!
    @IBOutlet weak var txtNotes: DTTextField!
    @IBOutlet weak var imgDog: UIImageView!

    private var viewModel: PetViewModel?
    var aryGender = ["Male","Female"]
    private var profilePicName: String = ""
    private var profilePicImage: UIImage?

    private lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTextFields()
        self.addProfileImgGesture()
        self.setLeftBarItemProfileImage()
        self.txtPetGender.inputView = self.pickerView
        self.viewModel = PetViewModel(view: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.configureDefaultNavigationBar(title: "Add a Pet")
    }
    func addProfileImgGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgDog.addGestureRecognizer(tapGestureRecognizer)
        imgDog.isUserInteractionEnabled = true;
    }
    
    @objc func imgTapped(_ sender:AnyObject){
        ImagePickerManager().pickImage(self){ (image, path, filename) in
            let circularImage = image.af.imageRoundedIntoCircle()
            self.imgDog.image = circularImage
            self.profilePicName = filename
            self.profilePicImage = image
        }
    }

    func setUpTextFields() {
        txtPetGender.addPadding(padding: .left(13))
        txtPetName.addPadding(padding: .left(13))
        txtPetAge.addPadding(padding: .left(13))
        txtPetColor.addPadding(padding: .left(13))
        txtNotes.addPadding(padding: .left(13))
    }
    func validateAddPetFields() -> Bool {
        guard !txtPetGender.text!.isEmptyStr
            else {
            txtPetGender.showError(message: ValidationText.petGenderMessage)
            return false
        }
        guard !txtPetColor.text!.isEmptyStr
            else {
            txtPetColor.showError(message: ValidationText.petColorMessage)
            return false
        }
        guard !txtNotes.text!.isEmptyStr
            else {
                txtNotes.showError(message: ValidationText.petNotesMessage)
            return false
        }

        guard !txtPetName.text!.isEmptyStr else {
            txtPetName.showError(message: ValidationText.petNameMessage)
            return false
        }

        guard !txtPetAge.text!.isEmptyStr else {
            txtPetAge.showError(message: ValidationText.petAgeMessage)
            return false
        }
        guard self.profilePicImage != nil else {
            self.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: "Please add your profile picture", action: nil) )
            return false
        }

                    return true
    }

    @IBAction func onBtnAddTapped(sender: Any) {
        if validateAddPetFields(), let petName = self.txtPetName.text, let petAge = self.txtPetAge.text,  let petGender = self.txtPetGender.text, let petColor = self.txtPetColor.text, let notes = self.txtNotes.text, let image = self.profilePicImage, let owerId = UserManager.sharedInstance.getUserDetials().user_id
                {
                SharedClass.sharedInstance.showLoader()

            DispatchQueue.global().async {
                SharedClass.sharedInstance.uploadImagePic(image: image , name: self.profilePicName, filePath: "") { (isUploaded) in
                            if isUploaded, let imageUrl = SharedClass.sharedInstance.cloudUrl {

                    self.viewModel?.performAddPet(body: ["name":petName, "age":petAge,"gender":petGender,"color":petColor,"notes":notes,"owner":owerId,"profile_picture": imageUrl], controller: self)
                }
            }
                    }
                    
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1
     }
     
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.aryGender.count
     }
    
     func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

         var pickerLabel = view as? UILabel;

         if (pickerLabel == nil)
         {
             pickerLabel = UILabel()

             pickerLabel?.font = UIFont(name: "Lato-Medium", size: 20)
             pickerLabel?.textAlignment = NSTextAlignment.center
         }

         pickerLabel?.text = self.aryGender[row]

         return pickerLabel!;
     }

     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtPetGender.text = self.aryGender[row]

     }


}

