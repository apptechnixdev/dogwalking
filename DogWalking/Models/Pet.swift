/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


*/

import Foundation
struct Pet : Codable {
	let id : String?
	let owner : String?
	let name : String?
	let age : String?
	let full_Name : String?
	let profile_picture : String?
	let pet_image : String?
    let color : String?
    let gender : String?
    let notes : String?

	enum CodingKeys: String, CodingKey {
		case id = "id"
		case owner = "owner"
		case name = "name"
		case age = "age"
		case full_Name = "Full_Name"
		case profile_picture = "profile_picture"
		case pet_image = "pet_image"
        case color = "color"
        case gender = "gender"
        case notes = "notes"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		owner = try values.decodeIfPresent(String.self, forKey: .owner)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		age = try values.decodeIfPresent(String.self, forKey: .age)
		full_Name = try values.decodeIfPresent(String.self, forKey: .full_Name)
		profile_picture = try values.decodeIfPresent(String.self, forKey: .profile_picture)
		pet_image = try values.decodeIfPresent(String.self, forKey: .pet_image)
        color = try values.decodeIfPresent(String.self, forKey: .color)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        notes = try values.decodeIfPresent(String.self, forKey: .notes)
	}

}
