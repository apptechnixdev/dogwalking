
import Foundation
struct WalkerRequest : Codable {
    let request_id : String?
    let walker_id : String?
    let ownerId : String?
    let ownername : String?
    let pet_name : String?
    let pet_image : String?
    let address : String?
    let pickup_date : String?
    let latitude : String?
    let longitude : String?
    let date_created : String?
    let duration : String?
    let profile_picture : String?

    enum CodingKeys: String, CodingKey {

        case request_id = "request_id"
        case walker_id = "walker_id"
        case ownerId = "ownerId"
        case ownername = "ownername"
        case pet_name = "pet_name"
        case pet_image = "pet_image"
        case address = "address"
        case pickup_date = "pickup_date"
        case latitude = "latitude"
        case longitude = "longitude"
        case duration = "duration"
        case date_created = "date_created"
        case profile_picture = "profile_picture"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        request_id = try values.decodeIfPresent(String.self, forKey: .request_id)
        walker_id = try values.decodeIfPresent(String.self, forKey: .walker_id)
        ownerId = try values.decodeIfPresent(String.self, forKey: .ownerId)
        ownername = try values.decodeIfPresent(String.self, forKey: .ownername)
        pet_name = try values.decodeIfPresent(String.self, forKey: .pet_name)
        pet_image = try values.decodeIfPresent(String.self, forKey: .pet_image)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        pickup_date = try values.decodeIfPresent(String.self, forKey: .pickup_date)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        date_created = try values.decodeIfPresent(String.self, forKey: .date_created)
        profile_picture = try values.decodeIfPresent(String.self, forKey: .profile_picture)
    }
}
