//
//  AccountSettingsViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/10/13.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class AccountSettingsViewController: UIViewController {

    @IBOutlet weak var txtFirstName: DTTextField!
    @IBOutlet weak var txtLastName: DTTextField!
    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet var imgProfile: UIImageView!

    private var profilePicName: String = ""
    private var profilePicImage: UIImage? = nil
    private var roleId = "1"
    @IBOutlet weak var txtCellNumber: DTTextField!
    @IBOutlet weak var txtAddress: DTTextField!
    @IBOutlet weak var btnSave: UIButton!
    private let viewModel = AccountSettingsViewModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.addProfileImgGesture()
        self.setUpTextFields()
        self.setUpUserDetails()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    
    func addProfileImgGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        imgProfile.isUserInteractionEnabled = true;
    }

    @objc func imgTapped(_ sender:AnyObject){
        ImagePickerManager().pickImage(self){ (image, path, filename) in
            let circularImage = image.af.imageRoundedIntoCircle()
            self.imgProfile.image = circularImage
            self.profilePicName = filename
            self.profilePicImage = image
        }
    }
    func setUpUserDetails(){
        txtFirstName.text = UserManager.sharedInstance.getUserDetials().firstname
        txtLastName.text = UserManager.sharedInstance.getUserDetials().surname
        txtEmail.text = UserManager.sharedInstance.getUserDetials().email
        txtCellNumber.text = UserManager.sharedInstance.getUserDetials().mobile_number
        txtAddress.text = UserManager.sharedInstance.getUserDetials().address
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: UserManager.sharedInstance.getUserDetials().profile_picture ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.imgProfile.bounds.size,
                radius: self.imgProfile.bounds.size.width / 2
            )
            self.imgProfile.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgProfile?.image = placeholderImage
        }


    }
    func setUpTextFields() {
        txtFirstName.addPadding(padding: .left(13))
        txtLastName.addPadding(padding: .left(13))
        txtEmail.addPadding(padding: .left(13))
        txtCellNumber.addPadding(padding: .left(13))
        txtAddress.addPadding(padding: .left(13))
    }
    
    func validateRegesterFields() -> Bool {
        guard !txtFirstName.text!.isEmptyStr else {
            txtFirstName.showError(message: ValidationText.firstNameMessage)
            return false
        }
        guard !txtLastName.text!.isEmptyStr else {
            txtLastName.showError(message: ValidationText.lastNameMessage)
            return false
        }
        guard !txtEmail.text!.isEmptyStr
            else {
            txtEmail.showError(message: ValidationText.emailMessage)
            return false
        }
        guard  txtEmail.isValidEmail(txtEmail.text ?? "") else {
            txtEmail.showError(message: ValidationText.emailMessage1)
            return false
        }
        guard !txtCellNumber.text!.isEmptyStr else {
            txtCellNumber.showError(message: ValidationText.cellNumberMessage)
            return false
        }
        guard !txtAddress.text!.isEmptyStr else {
            txtAddress.showError(message: ValidationText.addressMessage)
            return false
        }
        
        guard self.imgProfile.image != nil else {
            self.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: "Please add your profile picture", action: nil) )
            return false
        }

        return true
    }

    @IBAction func onBtnCloseTapped(sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func onBtnSaveTapped(sender: Any) {
        if validateRegesterFields(), let firstName = self.txtFirstName.text, let lastName = self.txtLastName.text,  let email = self.txtEmail.text, let cellNumber = self.txtCellNumber.text, let address = self.txtAddress.text, let memberId = UserManager.sharedInstance.getUserDetials().user_id {
            SharedClass.sharedInstance.showLoader()
            if let image = self.profilePicImage {
                DispatchQueue.global().async {
                    SharedClass.sharedInstance.uploadImagePic(image: image , name: self.profilePicName, filePath: "") { (isUploaded) in
                        if isUploaded, let imageUrl = SharedClass.sharedInstance.cloudUrl {
                            self.viewModel.performUpdateUserDetails(body: ["firstname":firstName, "surname":lastName, "email":email, "mobile_number":cellNumber, "address":address, "profile_picture": imageUrl, "id":memberId], controller: self)
                        }
                    }
                }
            }
            else {
                self.viewModel.performUpdateUserDetails(body: ["firstname":firstName, "surname":lastName, "email":email, "mobile_number":cellNumber, "address":address, "profile_picture": UserManager.sharedInstance.getUserDetials().profile_picture ?? "", "id":memberId], controller: self)
            }
            
        }

    }


}
