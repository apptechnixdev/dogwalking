//
//  ScheduleRepository.swift
//  DogWalking
//
//  Created by Admin on 2020/10/16.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

protocol ScheduleRepository {
func performScheduleWalk(params: [String: Any], completion:@escaping ((Error?)->Void))
    func getWalkerNewRequest(userId:String,completion:@escaping (([WalkerRequest]?, Error?)->Void))
    func getWalkerInProgressRequest(userId:String,completion:@escaping (([WalkerRequest]?, Error?)->Void))
    func updateWalkerNewRequest(params:[String:String],completion:@escaping ((Bool, Error?)->Void))

}
