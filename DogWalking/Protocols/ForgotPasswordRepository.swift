//
//  ForgotPasswordRepository.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/28.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

protocol ForgotPasswordRepository {
    func performForgotRequest(email:String, completion: @escaping ((Error?) -> Void)) 
}
