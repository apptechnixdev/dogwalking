//
//  UserManager.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/23.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
public class UserManager: NSObject {
    static let sharedInstance = UserManager()
    private var objUserDetials : UserDetails!
    
    func getUserDetials() -> UserDetails {
        return objUserDetials
    }
    
    func storeUserDetails(user:UserDetails) {
        self.objUserDetials = user
    }
    func updateUserDetails(user:UserDetails) {
        self.objUserDetials.firstname = user.firstname
        self.objUserDetials.surname = user.surname
        self.objUserDetials.mobile_number = user.mobile_number
        self.objUserDetials.email = user.email
        self.objUserDetials.address = user.address
        self.objUserDetials.profile_picture = user.profile_picture
    }

}
