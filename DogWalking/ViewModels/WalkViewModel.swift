//
//  WalkViewModel.swift
//  DogWalking
//
//  Created by Admin on 2021/01/04.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

class WalkViewModel: NSObject {
    private let objWalkRepository = WalkRepositoryImpl()
    
    func performStartWalk(body:[String:Any], objCurrentReqest : WalkerRequest?, controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
    objWalkRepository.performStartWalk(params: body) {  (error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                let objStartWalkViewController = storyBoard.instantiateViewController(withIdentifier: "StartWalkViewController") as! StartWalkViewController
                if let duration = objCurrentReqest?.duration, let nDuration = Int(duration) {
                    let fdurstion = String(nDuration * 60)
                    objStartWalkViewController.timeLeft = fdurstion.convertToTimeInterval()
                }
                objStartWalkViewController.objCurrentReqest = objCurrentReqest
                controller.navigationController?.pushViewController(objStartWalkViewController, animated: true)
            }
        }
    }
    func performEndWalk(body:[String:Any], controller: UIViewController) {
            SharedClass.sharedInstance.showLoader()
        objWalkRepository.performEndWalk(params: body) {  (error) in
                SharedClass.sharedInstance.dismissLoader()
                if let error = error {
                    controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
                }
                else {
                    controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Success", message: "Your walk  ended.", action: AlertAction(buttonTitle: "OK", handler: {
                        controller.navigationController?.popToRootViewController(animated: true)

                    })) )

                }
            }
        }

}



