
import UIKit
import MapKit

protocol MapDataSourceDelegate: class{    
    func refreshData()
}


class MapDataSource:NSObject{
    private var search:MKLocalSearch? =  nil
    private var searchCompleter = MKLocalSearchCompleter()
    private var places = [MKLocalSearchCompletion]()
    weak var delegate:MapDataSourceDelegate?
    
    override init() {
        super.init()
        searchCompleter.delegate = self
        if let currentLocation = LocationService.sharedInstance.currentLocation {
            searchCompleter.region = MKCoordinateRegion(center: currentLocation.coordinate, latitudinalMeters: 150, longitudinalMeters: 150) }
    }
    
    func locationCount() -> Int {
        return places.count
    }
    
    func locationAt(index:IndexPath) -> MKLocalSearchCompletion{
        return places[index.row]
    }
}

extension MapDataSource:UITableViewDataSource{
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = locationCount()
        return count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let item = locationAt(index: indexPath)
        cell.textLabel?.text = item.title
        cell.detailTextLabel?.text = item.subtitle
        return cell
    }
}

extension MapDataSource:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = locationAt(index: indexPath)
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = item.subtitle
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            self.searchCompleter.cancel()
            guard let response = response else {return}
            guard let item = response.mapItems.first else {return}
            let userInfo = ["mapItem" : item]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "storeAddress"), object: nil, userInfo: userInfo as [AnyHashable : Any])
        }
    }
}


extension MapDataSource:MKLocalSearchCompleterDelegate{
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        places = completer.results
        
        delegate?.refreshData()
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
    }
}


extension MapDataSource:UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
        
    }
}
