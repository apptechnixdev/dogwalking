//
//  LoginRepositoryImpl.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class LoginRepositoryImpl: NSObject {
}

extension LoginRepositoryImpl: LoginRepository {    
    func performLogin(params: [String:String], completion:@escaping ((UserDetails?, Error?)->Void)) {
        let objNetwork = NetworkCall(data: params, url: "apis/v1/login.php", service: .posts, method: .post)
        objNetwork.executePostQuery() {
            (result: Result<UserDetails,Error>) in
            switch result{
            case .success(let details):
                UserManager.sharedInstance.storeUserDetails(user: details)
                completion(details, nil)
            case .failure(let error1):
                completion(nil, error1)
            }
        }
    }
}
