//
//  ScheduleViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/09/18.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage
class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
}
class ScheduleViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var calendar: JKCalendar!
    @IBOutlet weak var imgDogMask: UIImageView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblSelectTime: UILabel!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!

    var objPet: Pet?
    var walkMinute: String = ""
    var arrData = [Pet]()
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [Pet]() 
    private var viewModel: ScheduleViewModel?
    var strScheduleDate : String = ""
    let aryTimes = ["07h00","07h30","08h00","08h30","09h00","09h30","10h00","10h30","11h00","11h30","12h00","12h30","04h00","04h30","05h00","05h30","06h00","06h30","07h00","07h30","08h00","08h30"]

    var selectDay: JKDay = JKDay(date: Date())
    let markColor = UIColor(hex: 0x0006E0)

    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.delegate = self
        calendar.dataSource = self
        
        calendar.textColor = UIColor(white: 0.25, alpha: 1)
        calendar.backgroundColor = UIColor.white
        
        calendar.isNearbyMonthNameDisplayed = false
        calendar.isScrollEnabled = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadPetsDetail()
        self.viewModel = ScheduleViewModel()

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let height = collectionView.collectionViewLayout.collectionViewContentSize.height
        if height < 128 {
            myCollectionViewHeight.constant = height }
        self.view.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadPetsDetail()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func loadPetsDetail() {
        self.arrData = PetManger.sharedInstance.getAllPets()
        if let pet = self.objPet, let index = self.arrData.firstIndex(where: {($0.id == pet.id)}) {
            self.arrSelectedData.append(pet)
            self.arrSelectedIndex.append(IndexPath(row: index, section: 0))
        }
        self.collectionView.reloadData()
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: objPet?.pet_image ?? "") {
            self.imgDogMask.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: nil,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgDogMask?.image = placeholderImage
        }

    }
    
    @IBAction func onBtnSubmitTapped(sender: Any) {
    let petsIds = arrSelectedData.compactMap { $0.id }
      if let address = UserManager.sharedInstance.getUserDetials().address,
         let lat = UserManager.sharedInstance.getUserDetials().latitude,
        let long = UserManager.sharedInstance.getUserDetials().longitude,
        let ownerId = UserManager.sharedInstance.getUserDetials().user_id{
        self.viewModel?.performScheduleWalk(body: ["address": address, "latitude": lat, "longitude": long, "owner": ownerId, "pickup_date": self.strScheduleDate, "pets": petsIds, "duration":walkMinute], objPet: self.objPet, controller: self)
        }
    }
    
    @IBAction func onBtnBackTapped(sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return aryTimes.count
    }
   
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var pickerLabel = view as? UILabel;

        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()

            pickerLabel?.font = UIFont(name: "Lato-Medium", size: 20)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }

        pickerLabel?.text = aryTimes[row]

        return pickerLabel!;
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lblSelectTime.text = aryTimes[row]
        let time = aryTimes[row]
        if let hh = time.components(separatedBy: "h").first, let mm = time.components(separatedBy: "h").last {
            strScheduleDate = "\(selectDay.year)-\(selectDay.month)-\(selectDay.day) \(hh):\(mm):00" }
    }
}

extension ScheduleViewController: JKCalendarDelegate{
    
    func calendar(_ calendar: JKCalendar, didTouch day: JKDay){
        selectDay = day
        calendar.focusWeek = day < calendar.month ? 0: day > calendar.month ? calendar.month.weeksCount - 1: day.weekOfMonth - 1
        calendar.reloadData()
    }
}
extension ScheduleViewController: JKCalendarDataSource {
    
    func calendar(_ calendar: JKCalendar, marksWith month: JKMonth) -> [JKCalendarMark]? {
        
        let firstMarkDay: JKDay = JKDay(year: 2018, month: 1, day: 9)!
        let secondMarkDay: JKDay = JKDay(year: 2018, month: 1, day: 20)!
        
        var marks: [JKCalendarMark] = []
        if selectDay == month{
            marks.append(JKCalendarMark(type: .rectangle,
                                        day: selectDay,
                                        color: markColor))
        }
        if firstMarkDay == month{
            marks.append(JKCalendarMark(type: .rectangle,
                                        day: firstMarkDay,
                                        color: markColor))
        }
        if secondMarkDay == month{
            marks.append(JKCalendarMark(type: .rectangle,
                                        day: secondMarkDay,
                                        color: markColor))
        }
        return marks
    }
    
    func calendar(_ calendar: JKCalendar, continuousMarksWith month: JKMonth) -> [JKCalendarContinuousMark]?{
        let startDay: JKDay = JKDay(year: 2018, month: 1, day: 17)!
        let endDay: JKDay = JKDay(year: 2018, month: 1, day: 18)!
        
        return [JKCalendarContinuousMark(type: .dot,
                                         start: startDay,
                                         end: endDay,
                                         color: markColor)]
    }
    
}

extension ScheduleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? CollectionViewCell)!
        let pet = self.arrData[indexPath.row]
        if arrSelectedIndex.contains(indexPath) {
            cell.contentView.backgroundColor = UIColor(hex: 0x0006E0)
            cell.lblTitle.textColor = .white
        }
        else {
            cell.contentView.backgroundColor = UIColor.lightGray
            cell.lblTitle.textColor = .black
        }
        cell.lblTitle.text = pet.name
        cell.layoutSubviews()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pet = arrData[indexPath.item]
        if arrSelectedIndex.contains(indexPath) {
            arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
            if let index = self.arrSelectedData.firstIndex(where: {($0.id == pet.id)}) {
                self.arrSelectedData.remove(at: index)
            }
        }
        else {
            arrSelectedIndex.append(indexPath)
            arrSelectedData.append(pet)
        }

        collectionView.reloadData()
    }
}

