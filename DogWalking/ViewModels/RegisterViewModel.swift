//
//  RegisterView.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class RegisterViewModel: NSObject {
    private let objRegisterRepository = RegisterRepositoryImpl()
    
    func performRegister(body:[String:String], controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objRegisterRepository.performRegister(params: body) { (error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Success", message: "Your account has been created.", action: AlertAction(buttonTitle: "OK", handler: {
                    let strEmail = body["email"] as String?
                    UDWrapper.setString(key: "isLogin", value: strEmail as NSString?)
                    let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                    let objLoginViewControler = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    objLoginViewControler.strEmail = strEmail
                    controller.navigationController?.pushViewController(objLoginViewControler, animated: true)

                     
                        
                })) )

            }
        }
    }

}
