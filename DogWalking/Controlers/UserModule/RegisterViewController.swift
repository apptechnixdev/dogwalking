//
//  RegisterViewController.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/21.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage
import MapKit


class RegisterViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var txtFirstName: DTTextField!
    @IBOutlet weak var txtLastName: DTTextField!
    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet weak var txtPassword: DTTextField! {
        didSet{
            txtPassword.rightViewMode = .always
            txtPassword.rightView = txtPassword.getHideShowView(target: self, selector: #selector(onBtnPasswordHideShow))
        }
    }
    @IBOutlet weak var txtCellNumber: DTTextField!
    @IBOutlet weak var txtAddress: DTTextField!
    @IBOutlet weak var btnRegisterNow: UIButton!
    @IBOutlet weak var btnTermsAndConditions: UIButton!
    @IBOutlet weak var btnDogWalker: UIButton!
    @IBOutlet weak var btnDogOwner: UIButton!
    @IBOutlet var imgProfile: UIImageView!

    private var roleId = "1"
    private let viewModel = RegisterViewModel()
    var objMapSearchVC: MapSearchVC!
    var mapItem: MKMapItem? = nil
    private var profilePicName: String = ""
    private var profilePicImage: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        txtAddress.delegate = self
        self.addAddressGesture()
        self.addProfileImgGesture()
        self.setLeftBarItemProfileImage()
        self.configureDefaultNavigationBar(title: "Create New Account")
        self.setUpTextFields()
        NotificationCenter.default.addObserver(self, selector: #selector(storeAddress(_:)), name: NSNotification.Name(rawValue: "storeAddress"), object: nil)
    }
    
    @objc func storeAddress(_ notification: NSNotification) {
        self.objMapSearchVC.navigationController?.dismiss(animated: true) {
            if let dict = notification.userInfo as NSDictionary?, let mapItem = dict["mapItem"] as? MKMapItem {
                var address = ""
                if let name = mapItem.placemark.title {
                    address = name
                }
                self.txtAddress.text = address
                self.mapItem = mapItem
            }
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtAddress {
            return false
        }
        return true
    }

    func addProfileImgGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgTapped))
        imgProfile.addGestureRecognizer(tapGestureRecognizer)
        imgProfile.isUserInteractionEnabled = true;
    }
    
    
    @objc func imgTapped(_ sender:AnyObject){
        ImagePickerManager().pickImage(self){ (image, path, filename) in
            let circularImage = image.af.imageRoundedIntoCircle()
            self.imgProfile.image = circularImage
            self.profilePicName = filename
            self.profilePicImage = image
        }
    }


    func addAddressGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(txtAddressTapped))
        txtAddress.addGestureRecognizer(tapGestureRecognizer)
        txtAddress.isUserInteractionEnabled = true;
    }
    @objc func txtAddressTapped(_ sender:AnyObject){
        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
        objMapSearchVC = storyBoard.instantiateViewController(withIdentifier: "MapSearchVC") as? MapSearchVC
        let navigation = UINavigationController(rootViewController: objMapSearchVC)
        navigation.modalPresentationStyle = .fullScreen
        self.navigationController?.present(navigation, animated: true, completion: nil)
    }
    
    func setUpTextFields() {
        txtFirstName.addPadding(padding: .left(13))
        txtLastName.addPadding(padding: .left(13))
        txtEmail.addPadding(padding: .left(13))
        txtPassword.addPadding(padding: .left(13))
        txtCellNumber.addPadding(padding: .left(13))
        txtAddress.addPadding(padding: .left(13))
    }
    
    func validateRegesterFields() -> Bool {
        guard !txtFirstName.text!.isEmptyStr else {
            txtFirstName.showError(message: ValidationText.firstNameMessage)
            return false
        }
        guard !txtLastName.text!.isEmptyStr else {
            txtLastName.showError(message: ValidationText.lastNameMessage)
            return false
        }
        guard !txtEmail.text!.isEmptyStr
            else {
            txtEmail.showError(message: ValidationText.emailMessage)
            return false
        }
        guard  txtEmail.isValidEmail(txtEmail.text ?? "") else {
            txtEmail.showError(message: ValidationText.emailMessage1)
            return false
        }
        guard !txtPassword.text!.isEmptyStr else {
            txtPassword.showError(message: ValidationText.passwordMessage)
            return false
        }
        guard !txtPassword.text!.isUpper else {
            txtPassword.showError(message: ValidationText.caseSensitivePassword)
            return false
        }
        guard !txtCellNumber.text!.isEmptyStr else {
            txtCellNumber.showError(message: ValidationText.cellNumberMessage)
            return false
        }
        guard !txtAddress.text!.isEmptyStr else {
            txtAddress.showError(message: ValidationText.addressMessage)
            return false
        }
        guard btnTermsAndConditions.isSelected else {
            self.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: "Please select Terms of use", action: nil) )

            return false
        }
        guard self.profilePicImage != nil else {
            self.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: "Please add your profile picture", action: nil) )
            return false
        }

        return true
    }
    
    @IBAction func onBtnRegisterNowTapped(sender: Any) {
        if validateRegesterFields(), let firstName = self.txtFirstName.text, let lastName = self.txtLastName.text,  let email = self.txtEmail.text, let password = self.txtPassword.text, let cellNumber = self.txtCellNumber.text, let image = self.profilePicImage, let address = self.txtAddress.text {
            SharedClass.sharedInstance.showLoader()
            var lat = "000"
            var long = "000"
            if self.mapItem != nil {
                lat = "\(self.mapItem?.placemark.location?.coordinate.latitude ?? 0)"
                long = "\(self.mapItem?.placemark.location?.coordinate.longitude ?? 0)"
            }
            else {
                let geoCoder = CLGeocoder()
                geoCoder.geocodeAddressString(address) { (placemarks, error) in
                    guard
                        let placemarks = placemarks,
                        let location = placemarks.first?.location
                    else {return}
                    lat = "\(location.coordinate.latitude)"
                    long = "\(location.coordinate.longitude)"
                }
            }

            DispatchQueue.global().async {
                SharedClass.sharedInstance.uploadImagePic(image: image , name: self.profilePicName, filePath: "") { (isUploaded) in
                        if isUploaded, let imageUrl = SharedClass.sharedInstance.cloudUrl {
                self.viewModel.performRegister(body: ["firstname":firstName, "surname":lastName, "email":email, "mobile_number":cellNumber, "address":address, "latitude": lat, "longitude": long, "password":password, "role_id":self.roleId, "profile_picture": imageUrl], controller: self)
                }
                        else {
                            SharedClass.sharedInstance.dismissLoader()
                    }
            }
        }
    }
    }

    @IBAction func onBtnRoleTapped(sender: UIButton) {
        if btnDogWalker == sender {
            self.roleId = "1"
            btnDogWalker.isSelected  = !btnDogWalker.isSelected
            btnDogOwner.isSelected = false
        }
        else {
            self.roleId = "2"
            btnDogOwner.isSelected  = !btnDogOwner.isSelected
            btnDogWalker.isSelected = false
        }
     }

    @IBAction func onBtnTappedTermsAndConditions(sender: Any) {
        btnTermsAndConditions.isSelected  = !btnTermsAndConditions.isSelected
     }
    
    @objc func onBtnPasswordHideShow(sender: UIButton) {
        sender.isSelected  = !sender.isSelected
        self.txtPassword.isSecureTextEntry = !self.txtPassword.isSecureTextEntry
    }
}
