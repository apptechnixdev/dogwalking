//
//  File.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/24.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit

struct GraphicColors {
    static let appGreenColor = UIColor(hex: 0x33BD42)
    static let appLightGrayColor = UIColor(hex: 0x626262)
    static let appDarkGrayColor = UIColor(hex: 0x202224)
}

struct ValidationText {
    static let firstNameMessage = NSLocalizedString("First name is required.", comment: "")
    static let lastNameMessage = NSLocalizedString("Last name is required.", comment: "")
    static let emailMessage = NSLocalizedString("Email is required.", comment: "")
    static let emailMessage1 = NSLocalizedString("Enter valid Email address.", comment: "")
    static let passwordMessage = NSLocalizedString("Password is required.", comment: "")
    static let caseSensitivePassword = "The password you provided is not valid. Please double check it. Remember, passwords are case-sensitive."

    static let cellNumberMessage = NSLocalizedString("Cell Number is required.", comment: "")
     static let addressMessage = NSLocalizedString("Address is required.", comment: "")
    static let userNotFoundMessage = "The email address you have provided is not a member. To become a member, click on Not a member? Join Us?."
    static let dateMessage = NSLocalizedString("Date is required.", comment: "")
    static let petNotesMessage = NSLocalizedString("Notes is required.", comment: "")
    static let petNameMessage = NSLocalizedString("Pet name is required.", comment: "")
    static let petAgeMessage = NSLocalizedString("Pet age is required.", comment: "")
    static let petColorMessage = NSLocalizedString("Pet color is required.", comment: "")
    static let petGenderMessage = NSLocalizedString("Pet gender is required.", comment: "")


}
