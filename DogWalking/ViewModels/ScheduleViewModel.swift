//
//  ScheduleViewModel.swift
//  DogWalking
//
//  Created by Admin on 2020/10/16.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ScheduleViewModel: NSObject {
        private let objScheduleWalkRepository = ScheduleRepositoryImpl()
        
    func performScheduleWalk(body:[String:Any], objPet: Pet?, controller: UIViewController) {
            SharedClass.sharedInstance.showLoader()
            objScheduleWalkRepository.performScheduleWalk(params: body) { (error) in
                SharedClass.sharedInstance.dismissLoader()
                if let error = error {
                    controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
                }
                else {
                        let storyBoard = UIStoryboard(name:"Main", bundle: nil)
                        let objThankYouViewController = storyBoard.instantiateViewController(withIdentifier: "ThankYouViewController") as! ThankYouViewController
                   // let duration = String(Int(body["duration"] as? String ?? "")! * 60)
                   // objThankYouViewController.timeLeft = duration.convertToTimeInterval()
                            objThankYouViewController.objPet = objPet
                        controller.navigationController?.pushViewController(objThankYouViewController, animated: true)
                }
            }
        }

    }


extension String {
    func convertToTimeInterval() -> TimeInterval {
        guard self != "" else {
            return 0
        }
        var interval:Double = 0

        let parts = self.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        return interval
    }
}
