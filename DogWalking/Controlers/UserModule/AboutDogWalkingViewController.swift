//
//  AboutDogWalkingViewController.swift
//  DogWalking
//
//  Created by Admin on 2021/01/19.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

class AboutDogWalkingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }

    
    
    @IBAction func onBtnBackTapped(sender: UIButton) {
        if let navigation = SharedClass.sharedInstance.getSideMenuController().contentController as? UINavigationController {
            navigation.popToRootViewController(animated: true)
        }

    }

    

}
