//
//  DogProfileViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/09/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage
protocol HistoryView {
func setHistory(history: [WalkHistory])
}

class HistoryCell: UICollectionViewCell {
    @IBOutlet weak var imgRootPicture: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblArea: UILabel!
}

class DogProfileViewController: UIViewController {
    @IBOutlet weak var imgPetProfile: UIImageView!
    @IBOutlet weak var lblPetName: UILabel!
    @IBOutlet weak var lblPetDetails: UILabel!
    @IBOutlet weak var lblPetAge: UILabel!
    @IBOutlet weak var lblPetActivity: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var aryHistory: [WalkHistory] = []
    var objHistoryViewModel: HistoryViewModel?
    var objPet: Pet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureNavigationBar(largeTitleColor: .black, backgoundColor: .white, tintColor: .white, title: "Profile", preferredLargeTitle: false)
        self.setLeftBarItemProfileImage()
        self.configureDogDetails()
        self.objHistoryViewModel = HistoryViewModel(view: self)
        self.objHistoryViewModel?.getWalkHistory(controller: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    func configureDogDetails() {
        if let dogName = objPet?.name, let dogAge = objPet?.age, let dogNotes = objPet?.name{
            self.lblPetName.text = dogName
            self.lblPetAge.text = "\(dogAge) years old"
            self.lblPetName.text = dogNotes
            self.lblPetActivity.text = "What would \(dogName) like to do today?"
        }
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: objPet?.pet_image ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.imgPetProfile.bounds.size,
                radius: self.imgPetProfile.bounds.size.width / 2
            )
            self.imgPetProfile.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgPetProfile?.image = placeholderImage
        }
    }
    
    @IBAction func onBtn20MinuteWalks(sender:UIButton) {
        self.showScheduleViewController(walkMin: 20)
    }

    @IBAction func onBtn30MinuteWalks(sender:UIButton) {
        self.showScheduleViewController(walkMin: 30)
    }
    
    func showScheduleViewController(walkMin:Int) {
        let objScheduleViewController = storyboard!.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        objScheduleViewController.objPet = objPet
        objScheduleViewController.walkMinute = "\(walkMin)"
        self.navigationController?.pushViewController(objScheduleViewController, animated: true)
    }
}
extension DogProfileViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 20, right: 20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 170, height: 198)
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aryHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HistoryCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCell", for: indexPath) as? HistoryCell)!
            let objHistory = self.aryHistory[indexPath.row]
        cell.lblArea.text =  objHistory.address
        if let walkDate = objHistory.date, let date = Date.FromString(walkDate) {
            let timeAgo:String=SharedClass.sharedInstance.timeAgoSinceDate(date, currentDate: Date().toLocalTime(), numericDates: true)
            cell.lblDate.text = timeAgo
        }

            let placeholderImage = UIImage(named: "20minswalk")!
            if let imgUrl = URL(string: objHistory.route_picture ?? "") {
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: cell.imgRootPicture.bounds.size,
                radius: cell.imgRootPicture.bounds.size.width / 2
            )
            cell.imgRootPicture.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
               // filter: filter,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            cell.imgRootPicture?.image = placeholderImage
        }

        return cell
    }
    
}

extension DogProfileViewController: HistoryView  {
    func setHistory(history: [WalkHistory]) {
        self.aryHistory = history
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }

    }
    
    
}

