
import UIKit
import CoreLocation

class MapSearchVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var searchController:UISearchController = UISearchController()
    
    private let manager = CLLocationManager()

    let dataSourece = MapDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        searchController =  UISearchController(searchResultsController:nil )
        searchController.searchBar.sizeToFit()
        searchController.searchBar.searchBarStyle = UISearchBar.Style.minimal
        searchController.searchBar.delegate = dataSourece
        searchController.isActive = true
        tableView.tableHeaderView = searchController.searchBar
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        title = "Location Search"
        
        dataSourece.delegate = self
        
        tableView.dataSource = dataSourece
        tableView.delegate = dataSourece
        self.setLeftBarItemProfileImage(self, #selector(onBtnRightBarItemTapped), imageName:"close1")
    }
    
    @objc func onBtnRightBarItemTapped() {
        self.navigationController?.dismiss(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension MapSearchVC:MapDataSourceDelegate{
    func refreshData() {
        self.tableView.reloadData()
    }
}



