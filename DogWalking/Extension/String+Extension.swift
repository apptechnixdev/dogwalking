//
//  String+Extension.swift
//  recyclego-ios
//
//  Created by Admin on 2020/08/29.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

extension String {
    func get12HoursFormattedDate() ->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        if let date = dateFormatterGet.date(from: self) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM yyyy hh:mm a"
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    func get24HoursFormattedDate() ->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMM yyyy hh:mm a"

        if let date = dateFormatterGet.date(from: self) {
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatterPrint.string(from: date)
        }
        return ""
    }
    
    func getDateFromFormatter(formatter: String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatter
        return dateFormatterGet.date(from: self)!
    }

}

extension NSString {
    func addBoldText(boldPartOfString: NSString, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
            let nonBoldFontAttribute = [NSAttributedString.Key.font:font!]
            let boldFontAttribute = [NSAttributedString.Key.font:boldFont!]
        let boldString = NSMutableAttributedString(string: self as String, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: self.range(of: boldPartOfString as String))
           return boldString
    }
    
}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String,font: UIFont!) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font ?? .none!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String,font: UIFont!) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font ?? .none!]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)
        return self
    }
}

extension Notification.Name {
    static let getWeatherData = Notification.Name("getWeatherData")
}

extension UIButton
{
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
    }
}
