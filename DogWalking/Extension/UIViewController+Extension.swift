//
//  UIViewController+Extension.swift
//
//  Created by Admin on 2020/06/07.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage


protocol SingleButtonDialogPresenter {
    func presentSingleButtonDialog(alert: SingleButtonAlert)
}


extension UIViewController {
    func configureNavigationBar(largeTitleColor: UIColor, backgoundColor: UIColor, tintColor: UIColor, title: String, preferredLargeTitle: Bool, fontSize : CGFloat = 34) {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: largeTitleColor, NSAttributedString.Key.font: UIFont(name: "JosefinSans-Bold", size: 34) ?? UIFont.boldSystemFont(ofSize: 34)]
            navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: largeTitleColor, NSAttributedString.Key.font: UIFont(name: "JosefinSans-Bold", size: 34) ?? UIFont.boldSystemFont(ofSize: 34)]
            navBarAppearance.backgroundColor = backgoundColor
           // navBarAppearance.textAlignment
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.compactAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance

            navigationController?.navigationBar.prefersLargeTitles = preferredLargeTitle
            navigationController?.navigationBar.isTranslucent = false
            navigationController?.navigationBar.tintColor = tintColor
            navigationItem.title = title

        } else {
            // Fallback on earlier versions
            navigationController?.navigationBar.barTintColor = backgoundColor
            navigationController?.navigationBar.tintColor = tintColor
            navigationController?.navigationBar.isTranslucent = false
            navigationItem.title = title
        }
    }
    
    func presentSingleButtonDialog(alert: SingleButtonAlert) {
        let alertController = UIAlertController(title: alert.title,message: alert.message,preferredStyle: .alert)
        if alert.action != nil {
            alertController.addAction(UIAlertAction(title: alert.action?.buttonTitle,style: .default,handler: { _ in alert.action?.handler?() })) }
        else {
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func configureDefaultNavigationBar(backgoundColor: UIColor = .white, tintColor: UIColor = UIColor(hex: 0x202224), title: String) {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: tintColor, NSAttributedString.Key.font: UIFont(name: "JosefinSans-Bold", size: 20) ?? UIFont.boldSystemFont(ofSize: 20)]
            navBarAppearance.backgroundColor = backgoundColor
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.tintColor = tintColor
            navigationItem.title = title

        } else {
            // Fallback on earlier versions
            navigationController?.navigationBar.barTintColor = backgoundColor
            navigationController?.navigationBar.tintColor = tintColor
            navigationController?.navigationBar.isTranslucent = false
            navigationItem.title = title
        }
    }
        
    func setRightBarItemImage(_ target: UIViewController? = nil,_ selector: Selector? = nil, imageName: String = "close1") {
        let contentView = UIView()
        contentView.frame = CGRect(x:0, y:0, width:44, height:44)
        let rightButton =  UIButton(type: .custom)
        let image = UIImage(named: imageName)
        rightButton.setImage(image, for:.normal)
        rightButton.frame = CGRect(x:-7, y:0, width:44, height:30)
        rightButton.imageView?.contentMode = .scaleAspectFit
        if let target = target, let selector = selector {
            rightButton.addTarget(target, action: selector, for: .touchUpInside) }
        else {
            rightButton.addTarget(self, action: #selector(onCloseBttonTapped), for: .touchUpInside)
        }
        contentView.addSubview(rightButton)
        let barButton = UIBarButtonItem(customView: contentView)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func onCloseBttonTapped() {
        self.navigationController?.popViewController(animated: true)
    }


    func setLeftBarItemProfileImage(_ target: UIViewController? = nil,_ selector: Selector? = nil, imageName: String = "Back Button") {
        let contentView = UIView()
        contentView.frame = CGRect(x:0, y:0, width:44, height:44)

        let leftButton =  UIButton(type: .custom)
        let image = UIImage(named: imageName)
        leftButton.setImage(image, for:.normal)
        leftButton.frame = CGRect(x:-7, y:0, width:44, height:30)
        leftButton.imageView?.contentMode = .scaleAspectFit
        if let target = target, let selector = selector {
            leftButton.addTarget(target, action: selector, for: .touchUpInside) }
        else {
            leftButton.addTarget(self, action: #selector(onBackBttonTapped), for: .touchUpInside)
        }
        contentView.addSubview(leftButton)
        let barButton = UIBarButtonItem(customView: contentView)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func onBackBttonTapped() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension UIViewController {
    
    func showInternetAlert() {
        let okAlert = SingleButtonAlert (
            title: "Could not connect to  network and try again later",
            message: "Failed to update information.",
            action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
        )
        presentSingleButtonDialog(alert: okAlert)
    }
    
    func showLocationAlert(){
        let locationAlert = SingleButtonAlert (
            title: "Could not find location",
            message: "Enable location for this App. Go to setting page",
            action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") }))
        presentSingleButtonDialog(alert: locationAlert)
    }
}
extension UIView{
     func blink() {
         self.alpha = 0.2
         UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
     }
}

extension UIView {
    
    @IBInspectable
    var cornerRadius1: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    @IBInspectable
    var borderWidth1: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor1: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

extension UIColor {
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}
extension UINavigationBar {
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.tintColor = .black 
    }
}

