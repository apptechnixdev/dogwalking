//
//  ForgotPasswordViewModel.swift
//  recyclego-ios
//
//  Created by Admin on 2020/07/28.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class ForgotPasswordViewModel: NSObject {
    private let objForgotPasswordRepository = ForgotPasswordRepositoryImpl()
    
    func performForgotRequest(email:String, controller: UIViewController) {
        SharedClass.sharedInstance.showLoader()
        objForgotPasswordRepository.performForgotRequest(email: email) { ( error) in
            SharedClass.sharedInstance.dismissLoader()
            if let error = error {
                controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Error", message: error.localizedDescription, action: nil) )
            }
            else {
                
                    controller.presentSingleButtonDialog(alert: SingleButtonAlert(title: "Alert", message: "Your reset pssword link sent to your register email.", action: AlertAction(buttonTitle: "OK", handler: {
                        controller.navigationController?.popViewController(animated: true)
                    })) )
                }
            }

        
    }
}
