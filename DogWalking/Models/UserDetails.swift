import Foundation
struct UserDetails : Codable {
	let message : String?
	let user_id : String?
	var firstname : String?
    var surname: String?
	var email : String?
	var address : String?
    let latitude : String?
    let longitude : String?
	var mobile_number : String?
	let access_token : String?
	let expires_in : Int?
	let creation_Time : Int?
	let expiration_Time : Int?
    var profile_picture : String?
    let role : String?

	enum CodingKeys: String, CodingKey {
		case message = "message"
		case user_id = "user_id"
		case firstname = "firstname"
        case surname = "surname"
		case email = "email"
		case address = "address"
        case latitude = "latitude"
        case longitude = "longitude"
		case mobile_number = "mobile_number"
		case access_token = "access_token"
		case expires_in = "expires_in"
		case creation_Time = "creation_Time"
		case expiration_Time = "expiration_Time"
        case profile_picture = "profile_picture"
        case role = "role"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		firstname = try values.decodeIfPresent(String.self, forKey: .firstname)
        surname = try values.decodeIfPresent(String.self, forKey: .surname)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		address = try values.decodeIfPresent(String.self, forKey: .address)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
		access_token = try values.decodeIfPresent(String.self, forKey: .access_token)
		expires_in = try values.decodeIfPresent(Int.self, forKey: .expires_in)
		creation_Time = try values.decodeIfPresent(Int.self, forKey: .creation_Time)
		expiration_Time = try values.decodeIfPresent(Int.self, forKey: .expiration_Time)
        profile_picture = try values.decodeIfPresent(String.self, forKey: .profile_picture)
        role = try values.decodeIfPresent(String.self, forKey: .role)
	}

}
