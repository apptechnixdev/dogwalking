//
//  ThankYouViewController.swift
//  DogWalking
//
//  Created by Admin on 2020/09/22.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AlamofireImage

class ThankYouViewController: UIViewController {
    @IBOutlet weak var imgDogMask: UIImageView!
    var objPet: Pet?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadPetsDetail()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.loadPetsDetail()
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    func loadPetsDetail() {
        let placeholderImage = UIImage(named: "DogMask")!
        if let imgUrl = URL(string: objPet?.pet_image ?? "") {
            self.imgDogMask.af.setImage(
                withURL: imgUrl,
                placeholderImage: placeholderImage,
                filter: nil,
                imageTransition: .crossDissolve(0.2)
            )
        }
        else{
            self.imgDogMask?.image = placeholderImage
        }

    }
    @IBAction func onBtnBackTapped(sender: UIButton) {
        if let navigation = SharedClass.sharedInstance.getSideMenuController().contentController as? UINavigationController {
            navigation.popToRootViewController(animated: true)
        }

    }
    

}
