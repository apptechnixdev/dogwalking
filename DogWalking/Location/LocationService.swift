//
//  LocationService.swift
//
//  Created by Admin on 2020/06/07.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService: NSObject, CLLocationManagerDelegate {
    static let sharedInstance: LocationService = {
        let instance = LocationService()
        return instance
    }()
    
    let locationManager: CLLocationManager
    var currentLocation: CLLocation?
    var locationDataArray: [CLLocation]
    var useFilter: Bool

    override init() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = 5
        locationManager.requestWhenInUseAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        locationDataArray = [CLLocation]()
        useFilter = true
        super.init()
        locationManager.delegate = self
    }
    
    static func isLocationServiceEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default: break
            }
        } else {
            print("Location services are not enabled")
            return false
        }
        return false
    }
    
    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }else{
            //tell view controllers to show an alert
            showTurnOnLocationServiceAlert()
        }
    }
    
    func stopUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled(){
            locationManager.stopUpdatingLocation()
        }
    }

        
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else {
            return
        }
        print(location)
        // singleton for get last(current) location
        currentLocation = location
        
        // use for real time update location
        if let newLocation = locations.last{
                print("(\(newLocation.coordinate.latitude), \(newLocation.coordinate.latitude))")
                var locationAdded: Bool
                if useFilter{
                    locationAdded = filterAndAddLocation(newLocation)
                }else{
                    locationDataArray.append(newLocation)
                    locationAdded = true
                }
                if locationAdded{
                    notifiyDidUpdateLocation(newLocation: newLocation)
            }
        }
    }
    
    func filterAndAddLocation(_ location: CLLocation) -> Bool {
        let age = -location.timestamp.timeIntervalSinceNow
        
        if age > 10{
            print("Locaiton is old.")
            return false
        }
        
        if location.horizontalAccuracy < 0{
            print("Latitidue and longitude values are invalid.")
            return false
        }
        
        if location.horizontalAccuracy > 100{
            print("Accuracy is too low.")
            return false
        }
        
        print("Location quality is good enough.")
        locationDataArray.append(location)
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if (error as NSError).domain == kCLErrorDomain && (error as NSError).code == CLError.Code.denied.rawValue{
            //User denied your app access to location information.
            showTurnOnLocationServiceAlert()
        }
    }
        
    public func locationManager(_ manager: CLLocationManager,
                                      didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            //You can resume logging by calling startUpdatingLocation here
        }
    }
        
    fileprivate func updateLocationDidFailWithError(_ error: NSError) {
        
    }
    
    func showTurnOnLocationServiceAlert() {
        NotificationCenter.default.post(name: Notification.Name(rawValue:"showTurnOnLocationServiceAlert"), object: nil)
    }
    
    func notifiyDidUpdateLocation(newLocation:CLLocation) {
        NotificationCenter.default.post(name: Notification.Name(rawValue:"didUpdateLocation"), object: nil, userInfo: ["location" : newLocation])
    }
}
