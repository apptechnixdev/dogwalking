import Foundation
import Alamofire

class NetworkCall : NSObject{
    
    enum services :String{
        case posts = "posts"
        case gets = "gets"

    }
    var parameters = Parameters()
    var headers = HTTPHeaders()
    var method: HTTPMethod!
    var url :String! = "https://happy-paws.co.za/dogwalking/"
    var encoding: ParameterEncoding! = JSONEncoding.default
    
    init(data: [String:Any] = [:], headers: [String:String] = [:],url :String?,service :services? = nil, method: HTTPMethod = .post, isJSONRequest: Bool = true){
        super.init()
        if data.count > 0 {
            data.forEach{parameters.updateValue($0.value, forKey: $0.key)}
        }

        headers.forEach({self.headers.add(name: $0.key, value: $0.value)})
        if url == nil, service != nil{
            self.url += service!.rawValue
        }else{
            self.url += url ?? ""
        }
        if !isJSONRequest{
            encoding = URLEncoding.default
        }
        self.method = method
       // print("Service: \(service?.rawValue ?? self.url ?? "") \n data: \(parameters)")
    }
    
    init(data: [String:Any] = [:],headers: [String:String] = [:], customUrl :String?, service:services? = nil, method: HTTPMethod = .post, isJSONRequest: Bool = true){
        super.init()
        if data.count > 0 {
            data.forEach{parameters.updateValue($0.value, forKey: $0.key)}
        }
        if headers.count > 0 {
            headers.forEach({self.headers.add(name: $0.key, value: $0.value)}) }

        if !isJSONRequest{
            encoding = URLEncoding.default
        }
        //
        if (customUrl?.hasPrefix("https"))! { // true
            self.url = customUrl ?? ""
        }
        else {
            self.url += customUrl ?? ""
        }

        self.method = method
       // print("Service: \(service?.rawValue ?? customUrl ?? "") \n data: \(parameters)")
    }
    
    
    func executePostQuery<T>(completion: @escaping (Result<T, Error>) -> Void) where T: Codable {
        guard Reachability.isConnectedToNetwork() else{
            UIApplication.topViewController()?.showInternetAlert()
            SharedClass.sharedInstance.dismissLoader()
            return
        }
        
        
        AF.request(url,method: method,parameters: parameters,encoding: encoding, headers: headers).responseData(completionHandler: { response in
            switch response.result{
            case .success(let res):
                if let code = response.response?.statusCode{
                    switch code {
                    case 200...299:
                        do {
                            completion(.success(payload: try JSONDecoder().decode(T.self, from: res)))
                        } catch let error {
                            print(String(data: res, encoding: .utf8) ?? "nothing received")
                            completion(.failure(error))
                        }
                    default:
                        do {
                            if let json = try JSONSerialization.jsonObject(with: res, options: []) as? [String : Any], let errorMessage = json["message"] as? String {
                                let error = NSError(domain: errorMessage, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                completion(.failure(error))
                            }
                            else {
                                let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                   completion(.failure(error))

                            }
                        }
                        catch {
                            let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                               completion(.failure(error))
                            }
                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func executeNonResultQuery(completion: @escaping (EmptyResult<Error>) -> Void) {
          guard Reachability.isConnectedToNetwork() else{
              UIApplication.topViewController()?.showInternetAlert()
              SharedClass.sharedInstance.dismissLoader()
              return
          }

        AF.request(url,method: method,parameters: parameters,encoding: encoding, headers: headers).responseData(completionHandler: {response in
            switch response.result{
            case .success(let res):
                if let code = response.response?.statusCode{
                    switch code {
                    case 200...299:
                        completion(.success)
                    default:
                     do {
                         if let json = try JSONSerialization.jsonObject(with: res, options: []) as? [String : Any], let errorMessage = json["message"] as? String {
                             let error = NSError(domain: errorMessage, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                             completion(.failure(error))
                         }
                         else {
                             let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                completion(.failure(error))

                         }
                     }
                     catch {
                         let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                            completion(.failure(error))
                         }

                    }
                }
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func executeGetReqest<T>(completion: @escaping (Result<T, Error>) -> Void) where T: Codable {
            guard Reachability.isConnectedToNetwork() else{
                UIApplication.topViewController()?.showInternetAlert()
                SharedClass.sharedInstance.dismissLoader()
                return
            }

        AF.request(url, method: .get, encoding: encoding, headers: headers)
            .responseData { response in

                switch response.result {

                case .success(let res):
                    if let code = response.response?.statusCode {
                        switch code {
                        case 200...299:
                            do {
                                let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                completion(.success(payload: obj))
                            } catch let error {
                                print(String(data: res , encoding: .utf8) ?? "nothing received")
                                completion(.failure(error))
                            }
                        default:
                             do {
                                let message = try self.parseJsonError(res: res)
                                let error = NSError(domain: message, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                completion(.failure(error))
                             }
                             catch {
                                 let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                    completion(.failure(error))
                                 }                        }
                    }                
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            }
    func canOpenURL(_ string: String?) -> Bool {
        guard let urlString = string,
            let url = URL(string: urlString)
            else { return false }

        if !UIApplication.shared.canOpenURL(url) { return false }

        let regEx = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    
    func parseJsonError(res: Data) throws -> String {
        do {
            let json = try JSONSerialization.jsonObject(with: res, options: []) as? [String : Any]
                return json?["message"] as? String ?? ""
            }
        catch let error {
            throw error
        }
    }


}
